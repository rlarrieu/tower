/******************************************************************************
* FILE       : triangular.h
* DESCRIPTION: Multivariate polynomials modulo triangular sets
* COPYRIGHT  : (C) 2019,  Robin Larrieu
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#ifndef _TOWER_TRIANGULAR__H_
#define _TOWER_TRIANGULAR__H_

#include "config.h"
#include "multivariate.h"
#include <vector>
#include "vector_tools.h"
#include "NTL/BasicThreadPool.h"

#ifdef PROFILE_MULMOD
#include "output_tools.h"
#endif

// sanity check
#ifdef PARALLEL_REDUCTION
#include "NTL/BasicThreadPool.h"
#ifndef NTL_THREADS
#warning "NTL's thread features are disabled; disabling PARALLEL_REDUCTION"
#endif
#endif

#ifdef PARALLEL_REDUCTION
#include <mutex>
#endif


template <typename BaseField>
class Triangular{

protected:
  typedef Multivariate<BaseField> Multi;
  typedef std::vector<Multi> System;

  // The triangular system itself
  System T;

  // degrees of the polynomials in the system
  deg_vec_t degrees;

#ifdef PARALLEL_REDUCTION
  std::mutex *outputLock;
#endif

  /*
  In the following, we define Kn := K[X0,...,X{n-1}] / (T0,...,T{n-1})
  */

public:
  Triangular(int alloc_size){
    T=System();
    T.reserve(alloc_size);
    degrees = deg_vec_t();
    degrees.reserve(alloc_size);
#ifdef PARALLEL_REDUCTION
    NTL::SetNumThreads(NUMTHREADS);
    outputLock = new std::mutex();
#endif
  }

  Triangular(): Triangular(TRIANGULAR_DEFAULT_ALLOC_SIZE) {}

#ifdef PARALLEL_REDUCTION
  ~Triangular(){
    delete outputLock;
  }
#endif

  inline int size() const { return T.size(); }

  // print the system for debugging purposes
  void Print() const{
    //printf("#Degrees:   "); print(degrees);
    printf("T=");
    print_vec(T,",\n  ", "\n", true);
  }

  Multi operator[] (int i){
    return T[i];
  }

  /*
  Reduction modulo the triangular set
  */

private:
  // Reduction in Kn[Xn,...] recursively: use the specific representation of T0,...,T{n-1}
#ifdef PARALLEL_REDUCTION
  inline void rem_rec(Multi& dst, const Multi& m, int n) const{
    rem_rec(dst, m, n, true);
  }

  virtual void rem_rec(Multi& dst, const Multi& m, int n, bool parallel) const {
    NTL::LogicError("Triangular::rem_rec not implemented");
  }
#define REM_REC(dst, m, n) rem_rec(dst, m, n, parallel)

#else
  virtual void rem_rec(Multi& dst, const Multi& m, int n) const {
    NTL::LogicError("Triangular::rem_rec not implemented");
  }
#define REM_REC(dst, m, n) rem_rec(dst, m, n)

#endif



protected:
  // Low-level hack for the following operations:
  //   aux1 := "the i-th coefficient in Kn of m"
  //   aux2 := "the normal form of aux1"
  //   "set the i-th coefficient in Kn of dst to aux2"
  // s1, s2, size_copy are the sizes of the actual data
  // assume aux1 and dst are initialized with the correct degrees and the required
  // space in dst.repr was allocated and initialized to zero
  // NOTE: m is not necessarily in Kn[Xn] so this is not equivalent to aux1 = m.coeff(i)
  //   for instance, if m in Kn[Xn,X{n+1}] with degree 3 in Xn and degree 2 in X{n+1},
  //   the 7th coefficient corresponds to degree 1 in X{n+1} and degree 2 in Xn
  inline void reduce_coeff(Multi& dst, const Multi& m, int n, size_type i,
               Multi& aux1, size_type s1, Multi& aux2, size_type s2,
               deg_vec_t& d, size_type size_copy) const {
    // get the coefficient of m
    NTL::clear(aux1.repr);
    aux1.repr.SetLength(size_copy);
    copy_range(m.repr, i*s1, size_copy, aux1.repr, 0);
    aux1.repr.normalize();
    // reduction
#ifdef PARALLEL_REDUCTION
    // only the top-level call is in parallel
    bool parallel = false;
#endif
    REM_REC(aux2, aux1, n);
    // copy in the result
    if (aux2 != 0)
      copy_range(aux2.repr, 0, NTL::deg(aux2.repr)+1, dst.repr, i*s2);
  }

#ifdef PARALLEL_REDUCTION
  inline void thread_safe_reduce_coeff
          (Multi& dst, const Multi& m, int n, size_type i,
           Multi& aux1, size_type s1, Multi& aux2, size_type s2,
           deg_vec_t& d, size_type size_copy) const {
    // get the coefficient of m
    NTL::clear(aux1.repr);
    aux1.repr.SetLength(size_copy);
    copy_range(m.repr, i*s1, size_copy, aux1.repr, 0);
    aux1.repr.normalize();
    // reduction
    // only the top-level call is in parallel
    bool parallel = false;
    REM_REC(aux2, aux1, n);
    // copy in the result
    if (aux2 != 0)
      thread_safe_copy_range(aux2.repr, 0, NTL::deg(aux2.repr)+1, dst.repr, i*s2, outputLock);
  }
#endif

public:
  // Compute the normal form in Kn[Xn, X{n+1},...]
  void rem(Multi& dst, const Multi& m, int n) const{
    // sanity check: degree in Xi must be at most 2*di - 1
    for (int i = 0; i < n && i < m.nb_vars(); i++){
      if (m.degrees[i] >= 2*degrees[i])
        NTL::LogicError("rem: degree too large");
    }
    if (n > size())
      return NTL::LogicError("rem: too many variables");
    if (n == 0){
      if (&dst != &m)
        dst = m;
      return;
    }
    int u = std::min(n, m.nb_vars());
    if (&dst == &m){
      Multi tmp;
      rem_rec(tmp, m, u);
      dst.swap(tmp);
      return;
    }
    else
      rem_rec(dst, m, n);
  }

  // Functional version
  inline Multi rem(const Multi& m, int n) const { Multi result; rem(result, m, n); return result; }
  // By default, assume n is the size of the system
  inline Multi rem(const Multi& m) const { Multi result; rem(result, m, size()); return result; }

  // Degrees for elements of Kn
  deg_vec_t degrees_red(int n) const{
    deg_vec_t res = degrees;
    for (int i = 0; i < n; i++){
      res[i] --;
    }
    return res;
  }

  // degrees for a polynomial in Kn[Xn] with degree d in Xn
  deg_vec_t degrees_red(int n, degree_type d) const{
    deg_vec_t res; res.resize(n+1);
    for (int i = 0; i < n; i++){
      res[i] = degrees[i];
      res[i] --; // degree[i] -1 may cause compiler warning
    }
    res[n] = d;
    return res;
  }

  // degrees for an element in normal form
  deg_vec_t degrees_NF() const{
    deg_vec_t res; res.resize(size());
    for (int i = 0; i < size(); i++){
      res[i] = degrees[i];
      res[i] --; // degree[i] -1 may cause compiler warning
    }
    return res;
  }

  /*
  Modular multiplication
  */

private:
  // check the appropriate size to compute a*b in Kn
  deg_vec_t degree_mul_mod(const deg_vec_t& da, const deg_vec_t& db, int n) const{
    if (n > size())
      NTL::LogicError("MulMod: index out of bounds");
    for (int i = 0; i < da.size() && i < n; i++){
      if (da[i] >= degrees[i])
        NTL::LogicError("Triangular::MulMod - Operand not in normal form");
    }
    deg_vec_t result = da;
    for (int i = 0; i < da.size() && i < db.size(); i++){
      if (i < n && da[i] + db[i] >= degrees[i])
        result[i] = 2*degrees[i] - 1;
      else
        result[i] += db[i];
    }
    return result;
  }

public:
  // Multiplication of two polynomials a,b in Kn[Xn,X{n+1},...]
  // (assuming a,b are already in normal form)
  // NOTE: This avoids one Kronecker substitution compared to rem(a*b)

  // dst = a*b mod T
  inline void MulMod(Multi& dst, const Multi& a, const Multi& b, int n) const{
#ifdef PROFILE_MULMOD
    chrono begin = now();
#endif
    deg_vec_t da = degree_mul_mod(a.degrees, b.degrees, n);
    deg_vec_t db = degree_mul_mod(b.degrees, a.degrees, n);
    deg_vec_t dd;
    if (da.size() < db.size())
      dd = db;
    else
      dd = da;

    Multi tmp;
    b.resizeTo(db,tmp);
    a.resizeTo(dd,dst);
#ifdef PROFILE_MULMOD
    chrono kronecker = now();
#endif
    dst.repr *= tmp.repr;
#ifdef PROFILE_MULMOD
    chrono multiply = now();
#endif
    rem(dst,dst,n);
#ifdef PROFILE_MULMOD
    chrono reduce = now();
    printf("# [PROFILE INFO] Triangular::MulMod : Kronecker %.3f ms, mult %.3f ms, rem %.3f ms\n",
           duration_ms(begin,kronecker),
           duration_ms(kronecker, multiply),
           duration_ms(multiply, reduce));
#endif
  }

  inline Multi MulMod(const Multi& a, const Multi& b, int n) const{
    Multi res;
    MulMod(res, a, b, n);
    return res;
  }

  // dst = a^2 mod T
  inline void SqrMod(Multi& dst, const Multi& a, int n) const{
    deg_vec_t da = degree_mul_mod(a.degrees, a.degrees, n);
    a.resizeTo(da,dst);
    NTL::sqr(dst.repr, dst.repr);
    rem(dst,dst,n);
  }

  inline Multi SqrMod(const Multi& a, int n) const{
    Multi res;
    SqrMod(res, a);
    return res;
  }

  // by default, k is the size of the system
  inline void MulMod(Multi& dst, const Multi& a, const Multi& b) const{
    MulMod(dst,a,b,size());
  }
  inline Multi MulMod(const Multi& a, const Multi& b) const{
    return MulMod(a,b,size());
  }

  inline void SqrMod(Multi& dst, const Multi& a) const{
    return SqrMod(dst,a,size());
  }
  inline Multi SqrMod(const Multi& a) const{
    return SqrMod(a,size());
  }


  /*
  Division and GCD
  */

protected: // p^(-1) mod Xn^d with p in Kn[Xn], P(0) = 1
  Multi InvModTrunc(const Multi& p, degree_type d, int n) const{
#ifdef STRONG_SANITY_CHECK
    if (p.nb_vars() != n+1 || size() < n)
      NTL::LogicError("InvModTrunc: inconsistant number of variables");
    if (d == 0)
      NTL::LogicError("InvModTrunc: inverse modulo 1 ?");
    if (p.coeff(0) != 1)
      NTL::LogicError("InvModTrunc: constant coefficient is not 1");
#endif
    //std::cout << "input=" << p << std::endl;
    Multi result(BaseField(1));
    Multi aux;
    degree_type u = 1;
    deg_vec_t deg = degrees_red(n,u); // degrees for Kn[Xn] with degree 1 in Xn
    result.resize(deg);
    while (u < d){
      //result = (2*result - p*result^2) rem Xn^u
      SqrMod(aux, result, n);
      MulMod(aux, aux, p, n);
      u *= 2;
      aux.trunc(u);
      result *= 2;
      result.trunc(u);
      subUnsafeLeft(result, aux);
    }
    // result = result mod X{n-1}^d
    result.trunc(d);
#ifdef STRONG_SANITY_CHECK
    // sanity check
    Multi check;
    MulMod(check, result, p, n);
    check.trunc(d);
    if (check != 1){
      std::cout << "input=" << p << std::endl;
      std::cout << "final_result=" << result << std::endl;
      std::cout << "check=" << check << std::endl;
      std::cout << "print 'InvModTrunc failed because check =', check, '(expected 1)'" << std::endl;
      NTL::LogicError("InvModTrunc: something went wrong");
    }
#endif
    return result;
  }


private:
  // Euclidean division for a,b in Kn[Xn]
  void DivRemModUnsafe(Multi& q, Multi& r, const Multi& a, const Multi& b, int n) const{
#ifdef STRONG_SANITY_CHECK
    if (a.nb_vars() != n+1 || b.nb_vars() != n+1 || n > size())
      NTL::LogicError("DivRem: inconsistent number of variables");
    if (b.degrees[n] > b.actual_degree()) // also ensures b != 0
      NTL::LogicError("DivRem: b has wrong degree");
#endif
    degree_type da = a.degrees[n];
    degree_type db = b.degrees[n];
    if (da < db){
      q = Multi();
      r = a;
      return;
    }
    if (n == 0){
      // shortcut: reduction of univariate polynomials
      da -= db;
      q = Multi(0, {da});
      r = Multi(0, {db});
      NTL::DivRem(q.repr, r.repr, a.repr, b.repr);
      r.trunc(db);
      return;
    }
    Multi c = b.coeff(db);
    InvMod(c,c);
    if (db == 0){
      MulMod(q,a,c,n);
      r = Multi();
      return;
    }
    r = reverse(b);
    MulMod(r,r,c,n);
    r = InvModTrunc(r, da-db+1, n);
    q = reverse(r);
    MulMod(q,q,c,n);
    MulMod(q,q,a,n);
    q.rshift(da); // q = q quo Xk^{da}
    MulMod(r,b,q,n);
    subUnsafeRight(a, r); // r = a-r
    r.trunc(db);
  }

  // Find s,q,r such that s*a = b*q+r with s in Kn and a,b,q,r in Kn[Xn], deg(r,Xn) < deg(b,Xn)
  void PseudoDivRemModUnsafe(Multi& s, Multi& q, Multi& r, const Multi& a, const Multi& b, int n) const{
#ifdef STRONG_SANITY_CHECK
    if (a.nb_vars() != n+1 || b.nb_vars() != n+1 || n > size())
      NTL::LogicError("DivRem: inconsistent number of variables");
    if (b.degrees[n] > b.actual_degree()) // also ensures b != 0
      NTL::LogicError("DivRem: b has wrong degree");
#endif
    degree_type da = a.degrees[n];
    degree_type db = b.degrees[n];
    if (da < db){
      s = Multi(1);
      q = Multi();
      r = a;
      return;
    }
    if (da - db > b.sizes[n]){ // TODO: which threshold?
      // Worth it to invert lc(b)
      s = Multi(1);
      DivRemModUnsafe(q,r,a,b,n);
      return;
    }
    // Pseudo division
    s = Multi(1);
    deg_vec_t dq = degrees_red(n,da-db); // degrees for Kn[Xn] with degree 1 in Xn
    q = Multi(0,dq);
    r = a;
    Multi d = b.coeff(db);
    Multi c; // leading coefficient of r
    degree_type dr = r.degree_Xn(n);
    while (dr >= db){
      c = r.lc();
      // q := d*q + c*Xn^(dr-db)
      MulMod(q,q,d,n); // q *= d
      q.setCoeff(dr - db, c); // q += c*Xn^(dr-db)
      // r := d*r - c*Xn^(dr-db)*b
      MulMod(r,r,d,n); // r *= d
      MulMod(c,c,b,n); // c *= b
      c.lshift(dr-db); // c *= Xn^(dr-db)
      subUnsafeLeft(r,c); // r -= c
      // update degree
      MulMod(s,s,d,n);
      r.normalize();
      dr = r.degree_Xn(n);
    }
  }

  // Euclidean division of Ti by b
  // (this should exploit the representation of Ti for better performance
  // so we canot use the above method)
  virtual void DivRemModUnsafe_Ti(Multi& q, Multi& r, int i, const Multi& b) const{
    NTL::LogicError("NotImplemented");
  }

  // Find s,q,r such that s*a = b*q+r with s in Kn and a,b,q,r in Kn[Xn], deg(r,Xn) < deg(b,Xn)
  void PseudoDivRemModUnsafe_Ti(Multi& s, Multi& q, Multi& r, int i, const Multi& b) const{
#ifdef STRONG_SANITY_CHECK
    if (b.nb_vars() != i+1 || i > this->size())
      NTL::LogicError("DivRem: inconsistent number of variables");
    if (b.degrees[i] > b.actual_degree() || b.degrees[i] > this->degrees[i])
      NTL::LogicError("DivRem: b has wrong degree");
    if (b.degrees[i] == 0)
      NTL::LogicError("DivRem: b shouldn't be a constant");
#endif
    degree_type dt = this->degrees[i];
    degree_type db = b.degrees[i];
    if (dt - db > b.sizes[i]){ // TODO: which threshold?
      // Worth it to invert lc(b)
      s = Multi(1);
      DivRemModUnsafe_Ti(q,r,i,b);
      return;
    }
    // make T[i] into standard Kronecker form;
    Multi Ti;
    T[i].resizeTo(degrees_red(i,degrees[i]), Ti);
    PseudoDivRemModUnsafe(s,q,r,Ti,b,i);
  }

  // For S and R in Kn[Xn]^(2x2), compute R := SxR, S := undefined
  void MatMulMod(Multi& S11, Multi& S12, Multi& S21, Multi& S22,
                 Multi& R11, Multi& R12, Multi& R21, Multi& R22,
                 int n) const{
    // Kronecker substitution
    deg_vec_t d; d.resize(n+1);
    for (int i = 0; i< n; i++)
      d[i] = 2*degrees[i] - 1;
    degree_type ds = max_of_four(S11.degree_Xn(n), S12.degree_Xn(n), S21.degree_Xn(n), S22.degree_Xn(n));
    degree_type dr = max_of_four(R11.degree_Xn(n), R12.degree_Xn(n), R21.degree_Xn(n), R22.degree_Xn(n));
    d[n] = ds + dr;
    S11.resize(d);
    S12.resize(d);
    S21.resize(d);
    S22.resize(d);
    R11.resize(d);
    R12.resize(d);
    R21.resize(d);
    R22.resize(d);
    // matrix multiplication then reduction
    polyMatMul(S11.repr, S12.repr, S21.repr, S22.repr,
           R11.repr, R12.repr, R21.repr, R22.repr,
           S11.sizes[n+1]);
    rem(R11,R11,n);
    rem(R12,R12,n);
    rem(R21,R21,n);
    rem(R22,R22,n);
  }

  // dst := a*b and appropriate Kronecker substitution c -> tmp1, d -> tmp2 (a,b,c,d in Kn[Xn])
  // XXX: dst, tmp1, tmp2 must not alias the inputs a,b,c,d
  void VecMulModAux(Multi& dst, Multi& tmp1, Multi& tmp2,
                    const Multi& a, const Multi& b,
                    const Multi& c, const Multi& d, int n) const{
#ifdef STRONG_SANITY_CHECK
    if (a.nb_vars() > n+1 || b.nb_vars() > n+1 || c.nb_vars() > n+1 || d.nb_vars() > n+1)
      NTL::LogicError("VecMulMod: inconsistent number of variables");
    for (int i = 0; i < n; i++){
      if ((a.nb_vars() >= n && a.degrees[i] >= degrees[i]) ||
          (b.nb_vars() >= n && b.degrees[i] >= degrees[i]) ||
          (c.nb_vars() >= n && c.degrees[i] >= degrees[i]) ||
          (d.nb_vars() >= n && d.degrees[i] >= degrees[i])    )
          NTL::LogicError("VecMulMod: degree problem");
    }
#endif
    // Kronecker substitution
    deg_vec_t degree_mult; degree_mult.resize(n+1);
    for (int i = 0; i< n; i++)
      degree_mult[i] = 2*degrees[i] - 1;
    degree_mult[n] = std::max(a.degree_Xn(n) + b.degree_Xn(n), c.degree_Xn(n) + d.degree_Xn(n));
    // dst := a*b
    a.resizeTo(degree_mult, tmp1);
    b.resizeTo(degree_mult, tmp2);
    dst = Multi(tmp1.repr * tmp2.repr, degree_mult);
    // Kronecker substitution for c*d
    c.resizeTo(degree_mult, tmp1);
    d.resizeTo(degree_mult, tmp2);
  }

  // dst = a*b + c*d for a,b,c,d in Kn[Xn]
  // XXX: dst must not alias an input -- maybe a or b is safe, but c,d are not
  inline void VecMulMod(Multi& dst, const Multi& a, const Multi& b, const Multi& c, const Multi& d, int n) const{
    Multi tmp1, tmp2;
    VecMulModAux(dst, tmp1, tmp2, a, b, c, d, n);
    dst.repr += tmp1.repr * tmp2.repr;
    // reduction
    rem(dst,dst,n);
    dst.normalize();
  }

  // dst = a*b - c*d for a,b,c,d in Kn[Xn]
  // XXX: dst must not alias an input -- maybe a or b is safe, but c,d are not
  inline void VecMulModSub(Multi& dst, const Multi& a, const Multi& b, const Multi& c, const Multi& d, int n) const{
    Multi tmp1, tmp2;
    VecMulModAux(dst, tmp1, tmp2, a, b, c, d, n);
    dst.repr -= tmp1.repr * tmp2.repr;
    // reduction
    rem(dst,dst,n);
    dst.normalize();
  }

  // Half GCD algorithm as auxiliary function (as described in Modern Computer Algebra)
  //(remark: we only care about the Bezout coefficients, but not each individual quotient)
  void HalfGCD(Multi& R11, Multi& R12, Multi& R21, Multi& R22, // the result matrix
               const Multi& a, const Multi& b, degree_type k, // input of the usual half-GCD algorithm
         int n) const{ // assume a,b are in Kn[Xn]
    // NOTE: a call to rshift preserves the number of variables or make the object become 0,
    // either way actual_degree gives the degree in the variable Xn.
    degree_type da = a.actual_degree();
    degree_type db = b.actual_degree();
    if (db < 0 || k < da-db){
      // R = Id
      R11 = Multi(BaseField(1));
      R12 = Multi();
      R21 = Multi();
      R22 = Multi(BaseField(1));
      return;
    }
#ifdef STRONG_SANITY_CHECK
    if ( n==0 )
      NTL::LogicError("HalfGCD shouldn't be called for univariate polynomials");
    if (a.nb_vars() != n+1 || b.nb_vars() != n+1 || n > size())
      NTL::LogicError("HalfGCD: inconsistent number of variables");
    if (da <= db)
      NTL::LogicError("HalfGCD: degree error");
#endif

    degree_type d = (k+1)/2;
    Multi aShift = a; Multi bShift = b;
    aShift.rshift(da - 2*d+2);
    bShift.rshift(da - 2*d+2);
    HalfGCD(R11,R12,R21,R22, aShift, bShift, d-1, n);


    aShift = a; bShift = b;
    aShift.rshift(da - 2*k);
    bShift.rshift(da - 2*k);

    // (rjm1 | rj) = R*(aShift | bShift)
    Multi rjm1, rj, aux;
    VecMulMod(rjm1, R11, aShift, R12, bShift, n);
    VecMulMod(rj,   R21, aShift, R22, bShift, n);

    // these have at most n variables, so degree_Xn(n) is the degree in Xn
    degree_type delta = R22.degree_Xn(n);
    degree_type njm1  = rjm1.degree_Xn(n);
    degree_type nj  = rj.degree_Xn(n);
    if (nj < 0 || k < delta + njm1 - nj)
      return;

    // Euclidean division of r{j-1} by rj
    Multi sj, qj, rjp1;
    rj.normalize();
    // can call the unsafe version because of the above checks
    PseudoDivRemModUnsafe(sj, qj, rjp1, rjm1, rj, n);

    // R = (0 1 | sj -qj) * R
    VecMulModSub(aux, R11, sj, R21, qj, n); // aux := R11*sj - R21*qj
    R11.swap(R21); // R11 := old_R21
    R21.swap(aux); // R21 := aux = R11*sj - R21*qj
    VecMulModSub(aux, R12, sj, R22, qj, n); // aux := R12*sj - R22*qj
    R12.swap(R22); // R12 := old_R22
    R22.swap(aux); // R22 := aux = R12*sj - R22*qj

    d = k-delta-njm1+nj;
    rj.rshift(nj - 2*d);
    rjp1.rshift(nj - 2*d);

    Multi S11, S12, S21, S22;
    HalfGCD(S11, S12, S21, S22, rj, rjp1, d, n);

    // R = S*R
    MatMulMod(S11, S12, S21, S22, R11, R12, R21, R22, n);

#ifdef STRONG_SANITY_CHECK
    // check that (rhm1|rh) := R*(a|b) verifies deg(rhm1 > deg(rh))
    // (result might be wrong, or this may cause infinite loop)
    Multi rhm1, rh;

    VecMulMod(rhm1, R11, a, R12, b, n); // rhm1 := R11*a + R12*b
    VecMulMod(rh,   R21, a, R22, b, n); // rh   := R21*a + R22*b

    // these have at most n variables, so degree_Xn(n) is the degree in Xn
    auto nhm1 = rhm1.degree_Xn(n);
    auto nh   = rh.degree_Xn(n);

    if (nhm1 <= nh){
      NTL::LogicError("HalfGCD: degree of r{h-1} is smaller than degree of rh");
    }
#endif
  }

public:
  // extended GCD of a and b (d = gdc(a,b) = a*s + b*t) in Kn[Xn]
  void XGCD(Multi& d, Multi& s, Multi& t,
            const Multi& a, const Multi& b,
            int n) const{
    if (a.nb_vars() > n+1 || b.nb_vars() > n+1 || n > size())
      NTL::LogicError("XGCD: inconsistent number of variables");
    if (n == 0){
      // shortcut: univariate polynomials
      d = Multi(BaseField(0), {a.degree_Xn(0)});
      s = Multi(BaseField(0), {b.degree_Xn(0)});
      t = Multi(BaseField(0), {a.degree_Xn(0)});
      NTL::XGCD(d.repr, s.repr, t.repr, a.repr, b.repr);
      d.normalize();
      s.normalize();
      t.normalize();
      return;
    }
    if (a.nb_vars() < n+1){
      d = a;
      s = Multi(1);
      t = Multi();
      return;
    }
    if (b.nb_vars() < n+1){
      d = b;
      s = Multi();
      t = Multi(1);
      return;
    }
    Multi u,v;
    degree_type da = a.actual_degree();
    degree_type db = b.actual_degree();
    if (da > db)
      HalfGCD(s, t, u, v, a, b, a.actual_degree(), n);
    else if (da < db)
      HalfGCD(t, s, u, v, b, a, b.actual_degree(), n);
    else{
      Multi lca = a.lc();
      Multi lcb = b.lc();
      VecMulModSub(d, lca, b, lcb, a, n); // tmp_d := lca * b - lcb*a
      // call HalfGCD with b and tmp_d (we know that deg(b) > deg(tmp_d))
      HalfGCD(t, s, u, v, b, d, b.actual_degree(), n);
      // d = t*b + tmp_d*s = (t-s*lca)*b + s*lcb*a
      Multi tmp;
      MulMod(tmp, s, lca);
      subUnsafeLeft(t, tmp); // t -= s*lca
      MulMod(s, s, lcb); // s *= lcb
    }
    VecMulMod(d, s, a, t, b, n); // d := s*a + t*b
    d.normalize();
    s.normalize();
    t.normalize();
  }

  // by default, consider the whole system (allow polynomials with one more variable)
  void XGCD(Multi& d, Multi& s, Multi& t,
        const Multi& a, const Multi& b) const{
    XGCD(d,s,t,a,b,size());
  }

  // dst = a^(-1) mod T
  void InvMod(Multi& dst, const Multi& a) const{
    int n = a.nb_vars();
    if (n > size())
      NTL::LogicError("InvMod: too many variables");
    if (&dst == &a){
      Multi tmp;
      InvMod(tmp, a);
      dst.swap(tmp);
      return;
    }
    if (n == 0){
      // a is in K
      dst = Multi(NTL::inv(NTL::coeff(a.repr,0)));
      return;
    }
    if (n == 1){
      // a is univariate
      degree_type deg = degrees[0]; deg--;
      dst = Multi(0,{deg});
      NTL::InvMod(dst.repr, a.repr, T[0].repr);
      return;
    }
    // Some sanity checks
    degree_type deg = a.degree_Xn(n-1);
    if (deg < 0)
      // a is zero
      NTL::LogicError("InvMod: not invertible");
    if (deg == 0){
      // a is in K{n-1}
      InvMod(dst, a.coeff(0));
      return;
    }
    if (deg > degrees[n-1])
      NTL::LogicError("InvMod: argument not in normal form");
    // cannot compute directly XGCD(Tn,a) because of the representation of Tn
    Multi q, r, s, t, d;
    // tmp_t*Ti = a*q+r (tmp_t is not used in the rest)
    if (deg == a.degrees[n-1]){
      PseudoDivRemModUnsafe_Ti(t,q,r,n-1,a);
    }
    else{
      s = a;
      s.normalize();
      PseudoDivRemModUnsafe_Ti(t,q,r,n-1,s);
    }
    XGCD(d, s, t, a, r, n-1);
    // d = s*a + t*r = (s-t*q)*a + tmp_t*t*Ti
    MulMod(q,q,t,n-1);
    subUnsafeLeft(s,q); // s -= q
    // d is not necessarily monic => the inverse is s/d
    if (d.degree_Xn(n-1) != 0)
      NTL::LogicError("InvMod: not ivertible");
    InvMod(d, d.coeff(0));
    MulMod(dst,s,d);
  }

  // Euclidean division in Kn[Xn]
  void DivRemMod(Multi& q, Multi& r, const Multi& a, const Multi& b, int n) const{
    if (a.nb_vars() > n+1 || b.nb_vars() > n+1 || n > size())
      NTL::LogicError("DivRemMod: inconsistent number of variables");
    if (b.nb_vars() < n+1){
      // b is a constant => exact division
      InvMod(q,b);
      MulMod(q,q,a,n);
      r = Multi();
      return;
    }
    if (a.degree_Xn(n) < b.degree_Xn(n)){
      // in particular, handles the case where a has less than n+1 variables
      q = Multi();
      r = a;
      return;
    }
    if (b.degree_Xn(n) == b.degrees[n]){
      DivRemModUnsafe(q,r,a,b,n);
    }
    else{
      Multi bp = b;
      bp.normalize();
      DivRemModUnsafe(q,r,a,bp,n);
    }
  }

  // by default, consider the whole system (allow polynomials with one more variable)
  void DivRemMod(Multi& q, Multi& r, const Multi& a, const Multi& b) const{
    DivRemMod(q,r,a,b,size());
  }


  // Euclidean division of Ti by b in Ki[Xi]
  void DivRemMod_Ti(Multi& q, Multi& r, int i, const Multi& b) const{
    if (i >= size())
      NTL::LogicError("DivRemMod_Ti: index out of bounds");
    if (i+1 > b.nb_vars()){
      // b is a "constant" => exact division
      Multi invB;
      InvMod(invB, b);
      rem(q, T[i], i); // handle possibly non-standard representation of Ti
      MulMod(q, q, invB, b.nb_vars());
      r = Multi();
      return;
    }
    if (i+1 < b.nb_vars() || b.degree_Xn(i) > degrees[i]){
      // i+1 < b.nb_vars()=j => Ti is a "constant" in K[X0,...,X{j-2}][X{j-1}]
      // b.degrees[i] > degrees[i] => Ti is already reduced with respect to b
      q = Multi();
      r = T[i];
      return;
    }
    if (b.degree_Xn(i) <= 0){
      // b is a "constant" => exact division
      Multi invB;
      InvMod(invB, b.coeff(0));
      rem(q, T[i], i); // handle possibly non-standard representation of Ti
      MulMod(q, q, invB, i);
      r = Multi();
      return;
    }
    if (b.degree_Xn(i) < b.degrees[i]){
      Multi bp = b;
      bp.normalize();
      DivRemModUnsafe_Ti(q, r, i, bp);
      return;
    }
    // Above tests ensure we can call the unsafe version now
    DivRemModUnsafe_Ti(q, r, i, b);
  }
};

#endif
