/******************************************************************************
* FILE       : compositum.h
* DESCRIPTION: Composita of ell-adic towers
* COPYRIGHT  : (C) 2019,  Luca De Feo
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#ifndef _COMPOSITUM__H_
#define _COMPOSITUM__H_

#include "config.h"
#include <assert.h>
#include <map>
#include <set>
#include <utility>
#include <memory>
#include "irred_tower.h"
#include <NTL/vector.h>

template<typename BaseField>
class Compositum {
public:
  typedef typename BaseField::poly_type Polynomial;
  typedef typename Polynomial::multiplier_type PolyMultiplier;
  typedef typename NTL::Vec<BaseField> Vector;
  typedef ElladicTower<BaseField> Tower;
  typedef std::map<long,long> Levels;

protected:
  std::map<long,std::unique_ptr<Tower>> towers;

private:
  std::map<double,Polynomial> moduli_cache;

public:
  Compositum<BaseField>() {
    Polynomial& Xm1 = moduli_cache[0];
    NTL::SetCoeff(Xm1, 0, -1);
    NTL::SetCoeff(Xm1, 1, 1);
  }

  inline long nb_towers() { return towers.size(); }
  bool add_tower(long ell) {
    return towers.insert(std::pair<long,std::unique_ptr<Tower>>(ell, std::move(std::unique_ptr<Tower>(new ElladicTower_Kummer<BaseField>(ell))))).second;
  }
  void add_tower(std::unique_ptr<Tower> tower) {
    return towers.insert(std::pair<long,std::unique_ptr<Tower>>(tower.ell, std::move(tower))).second;
  }

  Tower* operator [] (int ell) { return towers.at(ell); }

  // the polynomial to represent Fp^{prod(orders[i]^lev[i])}
  const Polynomial& modulus(const Levels& lev) {
    Polynomial& res = moduli_cache[key(lev)];
    if (deg(res) == -1) {
      // A binary splitting might be faster?
      // Also, it is wasteful to compose-prod with (X-1)
      for (auto& it: towers) {
        long h = at(lev, it.first);
        if (h > 0) {
          Levels rest = lev;
          rest[it.first] = 0;
          res = composed_product((*it.second)[h], modulus(rest));
          return res;
        }
      }
    }
    return res;
  }

  /*
   * Injection Fp^{prod(orders[i]^from[i])} -> Fp^{prod(orders[i]^to[i])}
   *
   * If orders[i] is missing for some i, then it is assumed orders[i] = 0
   *
   * Assume from[i] <= to[i] for all i
   */
  Polynomial move(const Polynomial& p, const Levels& from, const Levels& to) {
    Polynomial res;
    move(res, p, from, to, modulus(to));
    return res;
  }
  void move(Polynomial& res, const Polynomial& p,
            const Levels& from, const Levels& to, const Polynomial& Pto) {

    for (auto& it: towers) {
      // Find the first prime where from != to
      long ell = it.first;
      long f = at(from, it.first);
      long t = at(to, it.first);
      if (f != t) {
        Levels rest_to = to, rest_from = from;
        rest_from[ell] = rest_to[ell] = 0;

        // The polynomials defining the two composita
        const Polynomial& Plow = (*towers[ell])[f];
        const Polynomial& Phigh = (*towers[ell])[t];
        const Polynomial& Qlow = modulus(rest_from);
        const Polynomial& Qhigh = modulus(rest_to);
        long m = deg(Plow), n = deg(Qlow);
        long M = deg(Phigh), N = deg(Qhigh);

        // Switch to bivariate (x*⊗y)
        Vector bi = uni2biv(p, Plow, Qlow);

        Vector bi_tmp;
        bi_tmp.SetLength(M*n);
        // See the result as a polynomial in k[x][y]
        for (long i = 0; i < n; i++) {
          Vector c;
          slice(c, bi, i*m, (i+1)*m);
          // Convert x* -> x
          Polynomial C = dual2mono(c, Plow);
          // Lift the x part in the tower
          C = towers[ell]->move(C, f, t);
          for (long j = 0; j < M; j++)
            bi_tmp[j*n+i] = coeff(C, j);
        }

        bi.SetLength(M*N);
        // See the result as a polynomial in k[y][x]
        for (long i = 0; i < M; i++) {
          Polynomial C;
          slice(C, bi_tmp, i*n, (i+1)*n);
          // Apply recursively to the rest
          move(C, C, rest_from, rest_to, Qhigh);
          // Convert y -> y*
          Vector c = mono2dual(C, Qhigh);
          for (long j = 0; j < N; j++)
            bi[i*N+j] = c[j];
        }

        // Switch to univariate (y*⊗x)
        bi = biv2uni(bi, Phigh, Qhigh);

        // Convert z* -> z
        res = dual2mono(bi, Pto);
        return;
      }
    }

    // If we got out of the loop, then from == to
    res = p;
  }


private:
  /*
   * Algorithms for composita k[x]/P ⊗ k[y]/Q = k[z]/R.
   *
   * The convention, throughout, is that the variable x is associated
   * to the polynomial P, and y to Q.
   *
   * Elements can be represented on monomial or dual bases, and their
   * tensor products. So, x stands for monomial, x* stands for dual,
   * and x*⊗y stands for the tensor product of the bases x* and y.
   *
   * Polynomials in x or y are represented by the Polynomial type. All
   * other representations are represented by the Vector
   * type. Probably a bad choice.
   *
   * For bivariate things, we use the order to define the vector
   * layout: x⊗y means the coefficient of x^i ⊗ y^j is stored in
   * v[j*m+i]; y⊗x* means the coefficient of x*^i ⊗ y^j is stored in
   * v[i*n+j]; etc.
   */


  /*
   * Composed product of A and B, i.e. the polynomial whose roots are
   * the products of the roots of A and B.
   */
  Polynomial composed_product(const Polynomial &A, const Polynomial &B) {
    long m = deg(A);
    long n = deg(B);
    long d = 2*m*n;

    Vector vA = TraceVec(A);
    vA = trem(vA, A, d).first;
    Vector vB = TraceVec(B);
    vB = trem(vB, B, d).first;
    Vector vC;
    pmul(vC, vA, vB);

    // This is not optimal, but works in any characteristic.  Half as
    // many coefficients would be enough when p > mn.
    Polynomial res;
    MinPolySeq(res, vC, d/2);

    return res;
  }

  /*
   * Bivariate to univariate conversion: K[x,y]/(P(x),Q(y)) -> K[z]/R(z).
   *
   * Input is a n×m vector in the basis y*⊗x (dual basis on y,
   * monomial on x).
   *
   * Output is a vector in the basis z* (dual basis on z)
   */
  Vector biv2uni(const Vector& a, const Polynomial& P, const Polynomial& Q) {
    long m = deg(P);
    long n = deg(Q);
#ifdef ENABLE_COMPOSITUM_WARNING
    if (m > n) {
      std::cout << "Warning: degrees unoptimally balanced: " << m << " > " << n << "\n";
    }
#endif
    long d = m*n;
    Vector u = trem(TraceVec(P), P, d + m).first;

    Vector res;
    res.SetLength(d);

    for (long i = 0; i < m; i++) {
      Vector t;
      t.SetLength(n);
      for (long j = 0; j < n; j++)
        t[j] = a[i*n+j];
      t = trem(t, Q, d).first;
      for (long j = 0; j < d; j++)
        res[j] += t[j]*u[i+j];
    }

    return res;
  }

  /*
   * Univariate to bivariate conversion: K[z]/R(z) -> K[x,y]/(P(x),Q(y)).
   *
   * Input is a polynomial.
   *
   * Output is a m×n vector in the basis x*⊗y (dual basis on x,
   * monomial on y).
   */
  Vector uni2biv(const Polynomial& a, const Polynomial& P, const Polynomial& Q) {
    long m = deg(P);
    long n = deg(Q);
#ifdef ENABLE_COMPOSITUM_WARNING
    if (m > n) {
      std::cout << "Warning: degrees unoptimally balanced: " << m << " > " << n << "\n";
    }
#endif
    long d = m*n;
    Vector u = trem(TraceVec(P), P, d + m).first;

    Vector res;
    res.SetLength(m*n);

    for (long i = m-1; i >=0; i--) {
      Polynomial b;
      b.SetLength(d);
      for (long j = 0; j < d; j++)
        b[j] = coeff(a, j)*u[i+j];
      b.normalize();
      b = b % Q;
      for (long j = 0; j < b.rep.length(); j++)
        res[i+j*m]=b[j];
    }

    return res;
  }


  /*
   * Convert a from the monomial to the dual basis mod P.
   */
  Vector mono2dual(const Polynomial& a, const Polynomial& P) {
    return mono2dual(a, P, TraceVec(P));
  }
  Vector mono2dual(const Polynomial& a, const Polynomial& P, const Vector& trace) {
    PolyMultiplier A(a, P);
    Vector res = NTL::UpdateMap(trace, A, P);
    res.SetLength(deg(P));
    return res;
  }

  /*
   * Convert a from the dual to the monomial basis mod P.
   */
  Polynomial dual2mono(const Vector& a, const Polynomial& P) {
    return dual2mono(a, P, NTL::InvMod(NTL::diff(P), P));
  }
  Polynomial dual2mono(const Vector& a, const Polynomial& P, const Polynomial& diffinv) {
    long n = deg(P);
    Polynomial A;
    NTL::conv(A, a);
    return MulMod(reverse(NTL::MulTrunc(reverse(P), A, n), n-1), diffinv, P);
  }

  /*
   * Transposed reminder: given starting conditions v for linear
   * reccurence with minpoly P, output first degree terms of sequence.
   *
   * This forms also returns a multiplier, to be used in the other
   * form of trem.
   */
  std::pair<Vector,PolyMultiplier>
  trem(const Vector& v, const Polynomial& P, long degree) {
    long n = deg(P);
    std::pair<Vector,PolyMultiplier> res;
    res.first = v;
    Polynomial x(-P);
    NTL::SetCoeff(x, n, 0);
    res.second = PolyMultiplier(x, P);
    trem(res.first, P, res.second, degree);
    return res;
  }

  /*
   * Transposed reminder: given starting conditions v for linear
   * reccurence with minpoly P, output first degree terms of sequence.
   *
   * The multiplier X must contain X^n mod P.
   *
   * Writes result in place
   */
  void trem(Vector& v, const Polynomial& P, const PolyMultiplier& X, long degree) {
    long n = deg(P), stop = degree / n + (degree % n != 0);
    Vector c = NTL::UpdateMap(v, X, P);
    v.SetLength(degree);

    // this could be done more efficiently than using UpdateMap
    for (long i = 1; i < stop; i++) {
      for (long j = 0; j < c.length(); j++)
        v[n*i + j] = c[j];
      if (i < stop - 1)
        c = NTL::UpdateMap(c, X, P);
    }
  }

  void pmul(Vector& res, Vector& a, Vector& b) {
    long l = std::max(a.length(), b.length());
    res.SetLength(l);
    for (long i = 0; i < l; i++)
      res[i] = a[i]*b[i];
  }

  void slice(Vector& to, Vector& from, long start, long end) {
    to.SetLength(end-start);
    for (long i = 0; i < end-start; i++)
      to[i] = from[i+start];
  }
  void slice(Polynomial& to, Vector& from, long start, long end) {
    to.SetLength(end-start);
    for (long i = 0; i < end-start; i++)
      to[i] = from[i+start];
    to.normalize();
  }

  /***** Code for handling levels and caches *****/

  long at(const Levels& lev, long l) {
    try {
      return lev.at(l);
    } catch (std::out_of_range e) {
      return 0;
    }
  }

  double key(const Levels& lev) {
    double key = 0;
    for (auto& it: lev)
      key += it.second * std::log(it.first);
    return key;
  }
};

#endif // _COMPOSITUM__H_
