/******************************************************************************
* FILE       : irred_tower.h
* DESCRIPTION: Ell-adic towers over finite fields
* COPYRIGHT  : (C) 2019,  Luca De Feo
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#ifndef _IRRED_TOWER__H_
#define _IRRED_TOWER__H_

#include <assert.h>
#include <vector>
#include <utility>
#include <NTL/ZZ.h>
#include <NTL/GF2.h>
#include <NTL/lzz_p.h>
#include <NTL/ZZ_p.h>
#include <NTL/GF2X.h>
#include <NTL/lzz_pX.h>
#include <NTL/ZZ_pX.h>

template <typename BaseField>
class ElladicTower {
public:
  typedef typename BaseField::poly_type Polynomial;

protected:
  std::vector<Polynomial> polys;

public:
  long ell;

  long height() {
    return polys.size() - 1;
  }

  virtual void add_level() = 0;

  virtual Polynomial inject(const Polynomial& f, int from, int to) = 0;

  virtual Polynomial section(const Polynomial& f, int from, int to) = 0;

  virtual Polynomial move(const Polynomial& f, int from, int to) = 0;

  const Polynomial& operator[](int i) {
    for (long l = i - height(); l > 0; l--) {
      add_level();
    }
    return polys[i];
  }
}; // class ElladicTower



/*
 * Utility function to test the extension degree wrt the
 * characteristic:
 *
 * * -1: Artin-Schreier case ( ℓ = p, "bad reduction" )
 * *  1: Kummer case ( ℓ | (p-1) )
 * *  2: Conic case  ( ℓ | (p+1) )
 * *  0: Generic case ( good for elliptic curves )
 */
template <typename BaseField> int embedding_degree(long ell) {
  long pmodl = BaseField::modulus() % ell;
  return (pmodl == 0)
    ? -1
    : ( pmodl == 1 )
    ? 1
    : ( pmodl == ell - 1 )
    ? 2
    : 0;
}
template <>
int embedding_degree<NTL::GF2>(long ell) {
  return (ell == 2)
    ? -1
    : (ell == 3)
    ? 2
    : 0;
}

/*
 * Utility function to compute ell-adic valuation and cofactor of
 * integer N
 */
template <typename Int> std::pair<long,Int> val_cof(long ell, Int N) {
  assert(N != 0);
  long val = 0;
  while (N % ell == 0) {
    N /= ell;
    val++;
  }
  return std::pair<long,Int>(val, N);
}


template <typename BaseField>
class ElladicTower_Kummer : public ElladicTower<BaseField> {
public:
  typedef typename BaseField::poly_type Polynomial;

  ElladicTower_Kummer<BaseField>(long ell) {
    assert(embedding_degree<BaseField>(ell) == 1);
    this->ell = ell;
    // Generate a random element of order a maximal power of ell
    auto vc = val_cof(ell, BaseField::modulus() - 1);
    BaseField tmp(1);
    do {
      NTL::random(gen);
      if (IsZero(gen))
        continue;
      tmp = gen = NTL::power(gen, vc.second);
      for (long i = 0; i < vc.first - 1; i++) {
        tmp = NTL::power(tmp, ell);
      }
    } while (NTL::IsOne(tmp));
    assert(NTL::IsOne(power(tmp, ell)));
    // Store X-1 for the ground level
    Polynomial P;
    NTL::SetCoeff(P, 1, 1);
    NTL::SetCoeff(P, 0, -1);
    this->polys.push_back(P);
  }

  void add_level() {
    long i = this->height();
    Polynomial P;
    NTL::SetCoeff(P, NTL::power_long(this->ell, i+1));
    NTL::SetCoeff(P, 0, -gen);
    this->polys.push_back(P);
  }

  Polynomial inject(const Polynomial& f, int from, int to) {
    assert(from <= to);
    long exp = NTL::power_long(this->ell, to - from);
    Polynomial res;
    for (long i = 0; i <= NTL::deg(f); i++) {
      NTL::SetCoeff(res, exp*i, f[i]);
    }
    return res;
  }

  Polynomial section(const Polynomial& f, int from, int to) {
    assert(from >= to);
    long exp = NTL::power_long(this->ell, from - to);
    Polynomial res;
    for (long i = 0; i <= NTL::deg(f); i += exp) {
      NTL::SetCoeff(res, i / exp, f[i]);
      // check that the element belongs to the subfield
      for (long j = 1; j < exp && i+j <= NTL::deg(f); j++) {
        assert(NTL::IsZero(f[i+j]));
      }
    }
    return res;
  }

  Polynomial move(const Polynomial& f, int from, int to) {
    if (from <= to) {
      return inject(f, from, to);
    } else {
      return section(f, from, to);
    }
  }

private:
  BaseField gen;
}; // class ElladicTower_Kummer


#endif
