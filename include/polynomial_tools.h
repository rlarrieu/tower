/******************************************************************************
* FILE       : polynomial_tools.h
* DESCRIPTION: Additional utilities for NTL polynomials
* COPYRIGHT  : (C) 2019,  Robin Larrieu
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#ifndef _TOWER_POLYNOMIAL_TOOLS__H_
#define _TOWER_POLYNOMIAL_TOOLS__H_

#include "config.h"
#include <NTL/ZZ_pX.h>
#include <NTL/lzz_pX.h>
#include <NTL/GF2X.h>
#include <vector>
#include "vector_tools.h"


inline _ntl_ulong mask_before1(long n){
  // Mask corresponding to the bits strictly before n
  // in the current word (0 <= n < NTL_BITS_PER_LONG)
  return (((_ntl_ulong) 1) << n) - 1;
}

inline _ntl_ulong mask_before2(long n){
  // Mask corresponding to the bits before n (including n)
  // in the current word (0 <= n < NTL_BITS_PER_LONG)
  _ntl_ulong result = (((_ntl_ulong) 1) << n) - 1;
  result <<= 1;
  return result | 1;
}

/*
Copy the coefficients src[start : start+length-1] to dst[pos : pos+length-1]
*/
template <typename PolyType>
inline void copy_range_naive(const PolyType& src, const long start, const long length,
                             PolyType& dst, const long pos){

  for (long i = 0; i < length; i++)
    NTL::SetCoeff(dst, pos+i, NTL::coeff(src, start+i));
}

template <typename PolyType>
void copy_range(const PolyType& src, const long start, const long length,
                PolyType& dst, const long pos){
  copy_range_naive(src,start,length,dst,pos);
}

/*
Same operation but trying to optimize memory accesses with polynomials over GF2
(this feels like an awful and highly unsafe hack, but it is much faster)
*/
template <>
void copy_range<NTL::GF2X>
        (const NTL::GF2X& src, const long start, const long length,
        NTL::GF2X& dst, const long pos){
  // First and last relevant words
  long src_first = start / NTL_BITS_PER_LONG;
  long src_last  = ((start+length-1) / NTL_BITS_PER_LONG);
  long dst_first = pos   / NTL_BITS_PER_LONG;
  long dst_last  = ((pos  +length-1) / NTL_BITS_PER_LONG);
#ifdef STRONG_SANITY_CHECK
  // sanity checks (TODO: make my own exceptions to avoid conflicts?)
  if (src_last >= src.xrep.length())
    NTL::LogicError("copy_range<NTL::GF2X>: src overflow");
  if (dst_last >= dst.xrep.length())
    NTL::LogicError("copy_range<NTL::GF2X>: dst overflow");
#endif
  // Masks to discard non-relevant bits in src
  long src_shift = start % NTL_BITS_PER_LONG;
  long src_mask1 = ~mask_before1(src_shift); // remove bits before start (non included)
  long src_rem   = (start+length-1) % NTL_BITS_PER_LONG;
  long src_mask2 = mask_before2(src_rem);  // remove bits after start+length (included)
  // Masks to delete bits in dst that will be replaced
  long dst_shift = pos   % NTL_BITS_PER_LONG;
  long dst_mask1 = mask_before1(dst_shift); // keep only bits before pos (non included)
  long dst_rem   = (pos+length-1) % NTL_BITS_PER_LONG;
  long dst_mask2 = ~mask_before2(dst_rem);  // keep only bits after pos+length (included)

  // an auxiliary buffer to perform the required shifts
  long len_src = src_last - src_first + 1;
  long len_dst = dst_last - dst_first + 1;
  long len = len_src;
  if (len < len_dst)
    len = len_dst;
  _ntl_ulong *src_rep = src.xrep.rep;
  _ntl_ulong *dst_rep = dst.xrep.rep;
  _ntl_ulong aux[len];
  aux[len-1] = 0;
  for (long i=0; i < len_src; i++){
    aux[i] = src_rep[src_first+i];
  }
  // remove parts of src that are out of scope (before start or after start+length)
  aux[0] &= src_mask1;
  aux[len_src-1] &= src_mask2;
  if (src_shift < dst_shift){
    // shift everything left by dst_shift - src_shift
    long shift = dst_shift - src_shift;
    aux[len-1] <<= shift;
    for (long i = len-1; i > 0; i --){
      aux[i] |= aux[i-1] >> (NTL_BITS_PER_LONG - shift);
      aux[i-1] <<= shift;
    }
  }
  else if (src_shift > dst_shift){
    // shift everything right by src_shift - dst_shift
    long shift = src_shift - dst_shift;
    aux[0] >>= shift;
    for (long i = 0; i < len-1; i ++){
      aux[i] |= aux[i+1] << (NTL_BITS_PER_LONG - shift);
      aux[i+1] >>= shift;
    }
  }
  // add parts of dst before pos and after pos+length
  aux[0]   |= dst_rep[dst_first]  & dst_mask1;
  aux[len_dst-1] |= dst_rep[dst_last] & dst_mask2;
  for(long i = 0; i < len_dst; i ++){
    dst_rep[dst_first + i] = aux[i];
  }
}

/*
Thread-safe version: use additional lock parameter to avoid data races
*/

// XXX: default implementation looks thread-safe, but obvious data races for GF2X
// In practice, things go wrong for zz_p and ZZ_p as well if the mutex is not locked
template <typename PolyType>
inline void thread_safe_copy_range
    (const PolyType& src, const long start, const long length,
     PolyType& dst, const long pos, std::mutex * lock){
  lock -> lock();
  copy_range(src, start, length, dst, pos);
  lock -> unlock();
}


/*
Set all coefficients dst[start : start+length-1] to zero
*/
template <typename PolyType>
void fill_zeroes(PolyType& dst, const long start, const long length){
  auto zero = NTL::coeff(dst, -1); // get the zero element of the base field;
  for (long i = start; i < start+length; i++)
    NTL::SetCoeff(dst, i, zero);
}

/*
Same operation but trying to optimize memory accesses with polynomials over GF2
(this feels like an awful and highly unsafe hack, but it is much faster)
*/
template <>
void fill_zeroes<NTL::GF2X>(NTL::GF2X& dst, const long start, const long length){
  // conversions vec_GF2 -> WordVector
  long first = start / NTL_BITS_PER_LONG;
  long last  = (start+length-1)/NTL_BITS_PER_LONG;
  long shift = start % NTL_BITS_PER_LONG;
  long mask1 = mask_before1(shift); // keep only bits before start
  long rem   = (start+length-1) % NTL_BITS_PER_LONG;
  long mask2 = ~mask_before2(rem);  // keep only bits after start+length (included)
#ifdef STRONG_SANITY_CHECK
  // sanity checks (TODO: make my own exceptions to avoid conflicts?)
  if (last >= dst.xrep.length())
    NTL::LogicError("fill_zeroes<NTL::GF2X>: dst overflow");
#endif
  _ntl_ulong *dst_rep = dst.xrep.rep;
  if (first >= last)
    dst_rep[first] &= (mask1 | mask2);
  else{
    dst_rep[first] &= mask1;
    for (long i = first+1; i < last; i++)
      dst_rep[i] = 0;
    dst_rep[last] &= mask2;
  }
}


// copy an element of K[X0,...,X{n-1}] from src to dst at the given positions
template <typename PolyType>
void copy_multi(PolyType& dst,const PolyType& src, int n,
                size_type dst_offset, size_type src_offset,
                const deg_vec_t& dst_degrees, const size_vec_t& dst_sizes,  // degrees of dst in Xi, and size of K[X0,...,X{i-1}]
                const deg_vec_t& src_degrees, const size_vec_t& src_sizes){ // same but for src
  int a = src_degrees.size(); // number of variables in src
  int b = dst_degrees.size(); // number of variables in dst
  int m = n; // the actual number of variables to copy
  if (m > a)
    m = a;
#ifdef STRONG_SANITY_CHECK
  // sanity checks (TODO: make my own exceptions to avoid conflicts?)
  if (n > b)
    NTL::LogicError("copy_multi: attempting to copy more variables than dst has");
  if ((src_offset % src_sizes[m]) || (dst_offset % dst_sizes[n]))
    NTL::LogicError("copy_multi: bad alignment");
#endif
  // ensure there is enough space in dst
  if (dst_offset + dst_sizes[n] > NTL::deg(dst)){
    dst.SetLength(dst_offset + dst_sizes[n]);
  }
  // clear the destination
  fill_zeroes(dst, dst_offset, dst_sizes[n]);

  // nothing to do if src is zero
  long d =  NTL::deg(src);
  if (d < 0){
    dst.normalize();
    return;
  }

  /*
  try to copy as much contiguous memory as possible
  */
  int vars = 0;
  while ((vars < m) &&
         (src_degrees[vars] == dst_degrees[vars]))
    vars ++;
  size_type length = src_sizes[vars];
  // Kronecker substitutions in src/dst match for K[X0,...,X{vars-1}]
  if (vars < m){
    // copy coefficients in K[X0,...,X{vars}]
    length *= std::min(src_degrees[vars], dst_degrees[vars])+1;
    vars++;
  }

  if (vars == m){
    // just copy everything
    if (src_offset + length > d)
      length = d-src_offset+1;
    copy_range(src, src_offset, length, dst, dst_offset);
    dst.normalize();
    return;
    // NOTE: This handles the case m == 0
  }

  /*
  how far must we go in each direction
  */
  deg_vec_t copy_deg, progress;
  copy_deg.resize(m);
  progress.resize(m);
  for (int i = vars; i < m; i ++)
    copy_deg[i] = std::min(src_degrees[i], dst_degrees[i])+1;
  /*
  copy the memory sequentially
  */
  size_type end = src_offset + copy_deg[m-1] * src_sizes[m-1];
  if (end > d)
    end = d+1;
  while (src_offset+length <= end){
    copy_range(src, src_offset, length, dst, dst_offset);
    // find the next coefficient to copy
    progress[vars] += 1;
    int i = vars;
    while (i < m-1 && progress[i] == copy_deg[i]){
      progress[i] = 0;
      progress[i+1] += 1;
      i ++;
    }
    // we finished copying a coefficient in K[X0,...,X{i-1}]
    src_offset -= src_offset % src_sizes[i];
    src_offset += src_sizes[i];
    dst_offset -= dst_offset % dst_sizes[i];
    dst_offset += dst_sizes[i];
  }
  if (src_offset < end)
    copy_range(src, src_offset, end - src_offset, dst, dst_offset);

  // remove trailing zeroes if necessary
  dst.normalize();
}


/*
* For 2x2 matrices R and S, compute R := S*R, S:=undefined.
*/

// by default: naive multiplication (but use just one auxiliary variable
template <typename PolyType>
void polyMatMul(PolyType& S11, PolyType& S12,
                PolyType& S21, PolyType& S22, // coefficients of S
                PolyType& R11, PolyType& R12,
                PolyType& R21, PolyType& R22, // coefficients of R
                long d){ // a strict bound on the degree of the coefficients of S*R
  using namespace NTL;
  PolyType aux;
  mul(aux, S21, R11); // aux = S21 * R11
  mul(R11, R11, S11); // R11 = S11 * R11
  mul(S21, S21, R12); // S21 = S21 * R12
  mul(R12, R12, S11); // R12 = S11 * R12
  mul(S11, S22, R21); // S11 = S22 * R21
  mul(S22, S22, R22); // S22 = S22 * R22
  mul(R22, R22, S12); // R22 = S12 * R22
  mul(S12, S12, R21); // S12 = S12 * R21

  add(R11, R11, S12); // S11*R11 + S12*R21
  add(R12, R12, R22); // S11*R12 + S12*R22
  clear(R21); clear(R22); // just to be sure
  add(R21, aux, S11); // S21*R11 + S22*R21
  add(R22, S21, S22); // S21*R12 + S22*R22
}

// Perform the matrix product in the FFT model when available (zz_pX and ZZ_pX)
template <>
void polyMatMul<NTL::zz_pX>(NTL::zz_pX& S11, NTL::zz_pX& S12,
                            NTL::zz_pX& S21, NTL::zz_pX& S22,
                            NTL::zz_pX& R11, NTL::zz_pX& R12,
                            NTL::zz_pX& R21, NTL::zz_pX& R22,
                            long d){
  // This is essentially copy-pasted from NTL's source code (file lzz_pX1.cpp)
  // since there is no equivalent function in the public interface
  using namespace NTL;

  long k = NextPowerOfTwo(d);

  fftRep s11, s12, s21, s22, r1, r2, t1, t2;

  TofftRep(s11, S11, k); S11.kill();
  TofftRep(s12, S12, k); S12.kill();
  TofftRep(s21, S21, k); S21.kill();
  TofftRep(s22, S22, k); S22.kill();

  TofftRep(r1, R11, k); // R11.kill();
  TofftRep(r2, R21, k); // R21.kill();

  mul(t1, s11, r1);
  mul(t2, s12, r2);
  add(t1, t1, t2);
  FromfftRep(R11, t1, 0, d);

  mul(t1, s21, r1);
  mul(t2, s22, r2);
  add(t1, t1, t2);
  FromfftRep(R21, t1, 0, d);

  TofftRep(r1, R12, k); // R12.kill();
  TofftRep(r2, R22, k); // R22.kill();

  mul(t1, s11, r1);
  mul(t2, s12, r2);
  add(t1, t1, t2);
  FromfftRep(R12, t1, 0, d);

  mul(t1, s21, r1);
  mul(t2, s22, r2);
  add(t1, t1, t2);
  FromfftRep(R22, t1, 0, d);
}

template <>
void polyMatMul<NTL::ZZ_pX>(NTL::ZZ_pX& S11, NTL::ZZ_pX& S12,
                            NTL::ZZ_pX& S21, NTL::ZZ_pX& S22,
                            NTL::ZZ_pX& R11, NTL::ZZ_pX& R12,
                            NTL::ZZ_pX& R21, NTL::ZZ_pX& R22,
                            long d){
  // This is essentially copy-pasted from NTL's source code (file ZZ_pX1.cpp)
  // since there is no equivalent function in the public interface
  using namespace NTL;

  long k = NextPowerOfTwo(d);

  FFTRep s11, s12, s21, s22, r1, r2, t1, t2;

  ToFFTRep(s11, S11, k); S11.kill();
  ToFFTRep(s12, S12, k); S12.kill();
  ToFFTRep(s21, S21, k); S21.kill();
  ToFFTRep(s22, S22, k); S22.kill();

  ToFFTRep(r1, R11, k); // R11.kill();
  ToFFTRep(r2, R21, k); // R21.kill();

  mul(t1, s11, r1);
  mul(t2, s12, r2);
  add(t1, t1, t2);
  FromFFTRep(R11, t1, 0, d);

  mul(t1, s21, r1);
  mul(t2, s22, r2);
  add(t1, t1, t2);
  FromFFTRep(R21, t1, 0, d);

  ToFFTRep(r1, R12, k); // R12.kill();
  ToFFTRep(r2, R22, k); // R22.kill();

  mul(t1, s11, r1);
  mul(t2, s12, r2);
  add(t1, t1, t2);
  FromFFTRep(R12, t1, 0, d);

  mul(t1, s21, r1);
  mul(t2, s22, r2);
  add(t1, t1, t2);
  FromFFTRep(R22, t1, 0, d);
}

#endif
