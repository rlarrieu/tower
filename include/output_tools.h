/******************************************************************************
* FILE       : output_tools.h
* DESCRIPTION: Simple printing utilities
* COPYRIGHT  : (C) 2019,  Robin Larrieu
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#ifndef _TOWER_OUTPUT_TOOLS__H_
#define _TOWER_OUTPUT_TOOLS__H_

#include "config.h"
#include <iostream>
#include <map>
#include <string>
#include <chrono>
#include <stdarg.h>

#include "NTL/GF2XFactoring.h"
#include "NTL/lzz_pXFactoring.h"
#include "NTL/ZZ_pXFactoring.h"

/*
Some wrappers for printf that return valid sage code
*/
void sage_print(const char* fmt, ...){
  std::cout << "print '";
  va_list args; va_start(args, fmt);
  vprintf(fmt, args);
  va_end(args);
  std::cout << "'" << std::endl;
}

void sage_header1(const char* fmt, ...){
  printf("\nprint\n");
  sage_print("====================");
  va_list args; va_start(args, fmt);
  sage_print(fmt, args);
  va_end(args);
  sage_print("====================");
  printf("print\n\n");
}

void sage_header2(const char* fmt, ...){
  std::cout << "\nprint '----- ";
  va_list args; va_start(args, fmt);
  vprintf(fmt, args);
  va_end(args);
  std::cout << " -----'\n";
}

void sage_header3(const char* fmt, ...){
  std::cout << "print '# ";
  va_list args; va_start(args, fmt);
  vprintf(fmt, args);
  va_end(args);
  std::cout << "'\n";
}


void header1(const char* fmt, ...){
  printf("\n====================\n");
  va_list args; va_start(args, fmt);
  vprintf(fmt, args);
  va_end(args);
  printf("\n====================\n\n");
}
void header2(const char* fmt, ...){
  std::cout << "\n----- ";
  va_list args; va_start(args, fmt);
  vprintf(fmt, args);
  va_end(args);
  std::cout << " -----\n";
}
void header3(const char* fmt, ...){
  std::cout << "# ";
  va_list args; va_start(args, fmt);
  vprintf(fmt, args);
  va_end(args);
  std::cout << "\n";
}


/*
Main sage commands
*/

// declare a polynomial ring with nb_vars variables on ZZ / carac*ZZ
void declare_poly_ring(int nb_vars, int carac){
  printf("R.<X%d", nb_vars - 1);
  for (int i = nb_vars -2; i >= 0; i--)
    printf(",X%d",i);
  printf("> = PolynomialRing(GF(%d), order='lex')\n", carac);
}

// declare a sage variable with the given name
template <typename T>
void decl_sage_var(const char* name, const T& val){
  std::cout << name << " = " << val << std::endl;
}

// call the sage function run_test with given argument
void run_sage_test(const char* fmt, ...){
  std::cout << "run_test(";
  va_list args; va_start(args, fmt);
  vprintf(fmt, args);
  std::cout << ", '";
  va_end(args); va_start(args, fmt);
  vprintf(fmt, args);
  va_end(args);
  std::cout << "')\n";
}

// definition of some sage functions
std::__cxx11::string def_run_test="\
def run_test(cond, info_str):\n\
    print 'Test', count[1], '...',\n\
    count[0] += 1\n\
    count[1] += 1\n\
    if cond:\n\
        count[2] += 1\n\
        print 'OK'\n\
    else:\n\
        count[3] += 1\n\
        print 'FAIL ( test was', info_str,')'\n\
\n";

std::__cxx11::string def_is_reduc="\
def is_reduc(A, B, T):\n\
    return R(B).reduce(T) == R(A)\n\
\n";

std::__cxx11::string def_degree="\
def degree(p, var):\n\
    return R(p).degree(var)\n\
\n";

std::__cxx11::string def_matrix_equal_mod="\
def matrix_equal_mod(A, B, T):\n\
    for i in range(2):\n\
        for j in range(2):\n\
            if A[i,j].reduce(T) != B[i,j].reduce(T):\n\
                return False\n\
    return True\n\
\n";

// to be called at the beginning of main()
void start_script(){
  printf("count = [0,1,0,0]\n");
  std::cout << def_run_test << std::endl;
  std::cout << def_is_reduc << std::endl;
  std::cout << def_degree << std::endl;
  std::cout << def_matrix_equal_mod << std::endl;
}

// to reset the counter of current tests (but not the total)
void new_test_series(){
  printf("count[1]=1\n");
}

// to print a summary of the tests passed/failed
void end_script(){
  sage_print("\\nRan', count[0], 'tests |', count[2], 'passed |', count[3], 'failed");
}




/*
* Chrono utilities
*/
typedef std::chrono::time_point<std::chrono::system_clock> chrono;

chrono now() { return std::chrono::system_clock::now(); }

double duration_ms(chrono beg, chrono end){
  return (std::chrono::duration<double, std::milli>(end-beg)).count();
}


/*
Misc utilities
*/

void print_range(long start, long len){
  std::cout << "# ";
  for (long i=0; i < start; i++)
    std::cout << "  ";
  for (long i=0; i < len; i++)
    std::cout << "X ";
  std::cout << "\n";
}

void print_binary(_ntl_ulong i){
  _ntl_ulong shifted = i;
  for (int j = 1; j <= NTL_BITS_PER_LONG; j ++){
    std::cout << (shifted & 1);
    if (j % 8 == 0)
      std::cout << " ";
    shifted >>= 1;
  }
  std::cout << "\n";
}

void print_levels(const std::map<long,long>& levels) {
  bool print_star = false;
  for (auto& l: levels) {
    if (print_star)
      printf(" * %d^%d", l.first, l.second);
    else
      printf("%d^%d", l.first, l.second);
    print_star = true;
  }
  std::cout << std::endl;
}

void print_bench_result(chrono beg, chrono end, int nb_runs) {
  double time = duration_ms(beg, end);
  printf("  %d runs in %.4f ms (%.4f ms per run)\n",
         nb_runs, time, time / nb_runs);
}

void print_bench_result2(chrono beg, chrono end, int nb_runs) {
  double time = duration_ms(beg, end);
  printf("  Including %.4f ms (%.4f ms per run) for multiplications by b\n",
           time, time/nb_runs);
}

// bench univariate modular multiplication in degree d^n
// (to compare with multivariate in n variables and degree d)
template<typename Polynomial>
void bench_univariate_mulmod(int n, int d, int iter){
  typedef typename Polynomial::coeff_type BaseField;
  typedef typename Polynomial::modulus_type Modulus;
  printf("Comparing with univariate arithmetic\n");
  long deg = 1;
  for (int i = 0; i < n; i++) deg *= d;
  Polynomial f;
  NTL::random(f, deg); NTL::SetCoeff(f,deg,BaseField(1)); // make sure f is monic and degree d
  Modulus modulus = Modulus(f); // use the precomputed NTL structure
  Polynomial ua, ub;
  NTL::random(ua,deg-1); NTL::random(ub, deg-1);
  chrono beg = now();
  for (int i = 0; i < iter; i++){
    NTL::MulMod(ua,ua,ub,modulus);
  }
  chrono end = now();
  print_bench_result(beg,end,iter);
}

// bench univariate modular inversion in degree d
// (to compare with multivariate in n variables and degree d)
template<typename Polynomial>
void bench_univariate_invmod(long d, int iter){
  typedef typename Polynomial::coeff_type BaseField;
  typedef typename Polynomial::modulus_type Modulus;
  printf("Comparing with univariate arithmetic\n");
  Polynomial f;
  NTL::BuildIrred(f, d);
  Modulus modulus = Modulus(f); // use the precomputed NTL structure
  Polynomial ua, ub;
  NTL::random(ua,d-1); NTL::random(ub, d-1);
  chrono beg = now();
  for (int i = 0; i < iter; i++){
    NTL::InvMod(ua,ua,modulus);
    NTL::MulMod(ua,ua,ub,modulus);
  }
  chrono end = now();
  print_bench_result(beg,end,iter);
  beg = now();
  for (int i = 0; i < iter; i++){
    NTL::MulMod(ua,ua,ub,modulus);
  }
  end = now();
  print_bench_result2(beg,end,iter);
}

#endif

