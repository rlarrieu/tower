/******************************************************************************
* FILE       : tensor_product.h
* DESCRIPTION: Tensor product of composita using multivariate arithmetic
* COPYRIGHT  : (C) 2019,  Robin Larrieu
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#ifndef _TOWER_TENSOR_PRODUCT__H_
#define _TOWER_TENSOR_PRODUCT__H_

#include "config.h"
#include <vector>
#include <NTL/ZZ.h>
#include "compositum.h"
#include "triangular_univariate.h"
#include "multivariate.h"

#ifdef PROFILE_TENSOR_PRODUCT_MOVE
#include "output_tools.h"
#endif

/*
* Tensor product of composita
*/
template <typename BaseField>
class TensorProduct {
public:
  typedef typename BaseField::poly_type Polynomial;
  typedef Compositum<BaseField> Comp;
  typedef TriangularUnivariate<BaseField> System;
  typedef Multivariate<BaseField> Multi;
  typedef NTL::Vec<BaseField> Vector;
  typedef std::map<long,long> Lev;

private:
  typedef std::set<long> Ord;

  std::vector<Comp> composita; // the (ordered) sequence of composita
  std::vector<Ord>  orders; // the corresponding orders

public:
  TensorProduct<BaseField>(): composita(), orders() {}

  TensorProduct<BaseField>(int nb): composita(nb), orders(nb) {}

  // add an empty compositum
  void add_compositum() {
    composita.push_back(Comp());
    orders.push_back(Ord());
  }

  // add the ell-adic tower to the compositum number i
  void add_tower(long ell, unsigned int i) {
    for (Ord o: orders){ // check that ell is not already present
      if (o.count(ell)){
        std::cout << "# [Warning] TensorProduct.add_tower: " << ell
                  << "-adic tower already present, skipping\n";
        return;
      }
    }
    assert(i < composita.size());
    if (composita[i].add_tower(ell))
      orders[i].insert(ell);
  }

  // associate each pi^ei in levels to the correct compositum
  std::vector<Lev> split(Lev levels) {
    std::vector<Lev> result(orders.size());
    for (auto & l: levels){
      bool found = false;
      long ell = l.first;
      for (int i = 0; i < orders.size(); i++){
        if (orders[i].count(ell)){
          result[i].emplace(ell, l.second);
          found = true;
          break;
        }
      }
      if (!found) // TODO: add the tower to a compositum instead
        NTL::LogicError("No corresponding ell-adic tower");
    }
    return result;
  }

  // Generate the triangular system corresponding to prod(pi^ei)
  System system(Lev levels) {
    std::vector<Lev> v = split(levels);
    System result(v.size());
    for (int i = 0; i < v.size(); i ++)
      result.update(composita[i].modulus(v[i]));
    return result;
  }

  //Injection Fp^{prod(orders[i]^from[i])} -> Fp^{prod(orders[i]^to[i])}
  Multi move(const Multi& x, Lev from, Lev to) {
#ifdef PROFILE_TENSOR_PRODUCT_MOVE
    chrono begin = now();
#endif
    if (x.sizes.size() == 1) // x is in the base field
      return x;
    std::vector<Lev> vf = split(from);
    std::vector<Lev> vt = split(to);
    assert(x.nb_vars() <= vf.size());
    // find the degrees in each variable
    std::vector<long> df(x.nb_vars());
    std::vector<long> dt(x.nb_vars());
    long buffer_size = 1;
    for (int i = 0; i < x.nb_vars(); i++){
      long d1 = 1;
      long d2 = 1;
      for (auto& l: vf[i])
        d1 *= NTL::power_long(l.first, l.second);
      for (auto& l: vt[i])
        d2 *= NTL::power_long(l.first, l.second);
      df[i] = d1-1;
      dt[i] = d2-1;
      buffer_size *= d2;
    }
    // working buffer
    Vector in_buffer, out_buffer;
    NTL::VectorCopy(in_buffer, x.repr, deg(x.repr)+1);
    in_buffer.SetLength(buffer_size);
    out_buffer.SetLength(buffer_size);
    long stride  = 1;
#ifdef PROFILE_TENSOR_PRODUCT_MOVE
    chrono end_init = now();
    printf("# [PROFILE INFO] TensorProduct::move : init %.3f ms\n",
           duration_ms(begin,end_init));
#endif
    for (int i = 0; i < x.nb_vars(); i++){ // conversion for the variable Xi
      if (x.degrees[i] == 0 || df[i] == dt[i]){ // nothing to do
        dt[i] = x.degrees[i];
        stride  *= x.degrees[i]+1;
#ifdef PROFILE_TENSOR_PRODUCT_MOVE
        printf("# [PROFILE INFO] TensorProduct::move : skipping X%d\n",i);
#endif
        continue;
      }
#ifdef PROFILE_TENSOR_PRODUCT_MOVE
      chrono beg_Xi = now();
#endif
      const Polynomial m = composita[i].modulus(vt[i]);
      Polynomial work(x.degrees[i],1);
      Polynomial res(dt[i],1);
      // number of "coefficients" (each in K[X0,....,Xi])
      long nb_KX0i = last(x.sizes) / x.sizes[i+1];
      for (long j = 0; j < nb_KX0i; j++){
        long in_start  = j*(x.degrees[i]+1)*stride;
        long out_start = j*(dt[i]+1)*stride;
        for (long k = 0; k < stride; k++){
          for (long l = 0; l <= x.degrees[i]; l++)
            NTL::SetCoeff(work, l, in_buffer[in_start + k + l*stride]);
          composita[i].move(res, work, vf[i], vt[i], m);
          for (long l = 0; l <= dt[i]; l++)
            out_buffer[out_start + k + l*stride] = NTL::coeff(res,l);
        }
      }
      NTL::swap(in_buffer, out_buffer);
#ifdef PROFILE_TENSOR_PRODUCT_MOVE
      chrono end_Xi = now();
      double time_Xi = duration_ms(beg_Xi,end_Xi);
      long nb_move = nb_KX0i*stride;
      printf("# [PROFILE INFO] TensorProduct::move : X%d:", i);
      printf("%d Compositum::move (size %d -> %d) in %.3f ms (%.3f ms per call)\n",
        nb_move, df[i]+1, dt[i]+1, time_Xi, (time_Xi)/ nb_move);
#endif
      stride *= dt[i]+1;
    }
    Polynomial result;
    NTL::conv(result, in_buffer);
#ifdef PROFILE_TENSOR_PRODUCT_MOVE
    chrono end = now();
    printf("# [PROFILE INFO] TensorProduct::move : total %.3f ms\n",
           duration_ms(begin,end));
#endif
    return Multi(result, dt);
  }

  void print() const {
    std::cout << "Tensor product of " << composita.size() << " composita:\n";
    for (Ord o: orders){
      std::cout << " -> compositum for primes";
      for (auto & p: o)
        std::cout << " " << p;
      std::cout << "\n";
    }
  }
};

#endif // _TOWER_TENSOR_PRODUCT__H_
