/******************************************************************************
* FILE       : config.h
* DESCRIPTION: Customization options
* COPYRIGHT  : (C) 2019,  Robin Larrieu
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#ifndef _TOWER_CONFIG__H_
#define _TOWER_CONFIG__H_

/*
* Enable low-level sanity checks
* (use only for debugging as it may slow things down)
*/
//#define STRONG_SANITY_CHECK


/*
* Default allocated number of polynomials in a triangular system
*/
#define TRIANGULAR_DEFAULT_ALLOC_SIZE 5


/*
* Enable multithreading to reduce multivariate polynomials faster
*/
//#define PARALLEL_REDUCTION


/*
* Number of threads for parallel reduction
*/
#define NUMTHREADS 8

/*
* Print additional bench information for each subtask
* of multiplication modulo a triangular system
*/
//#define PROFILE_MULMOD

/*
* Print additional bench information for each subtask
* of computation of a field embedding
*/
//#define PROFILE_TENSOR_PRODUCT_MOVE

/*
* Enable warnings for unoptimally balanced degrees in composita
*/
//#define ENABLE_COMPOSITUM_WARNINGS

#endif
