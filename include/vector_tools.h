/******************************************************************************
* FILE       : vector_tools.h
* DESCRIPTION: Simple additional std::vector utilities
* COPYRIGHT  : (C) 2019,  Robin Larrieu
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#ifndef _TOWER_VECTOR_TOOLS__H_
#define _TOWER_VECTOR_TOOLS__H_

#include <vector>
#include <string>

typedef long  degree_type;
typedef std::vector<degree_type> deg_vec_t;
typedef long  size_type;
typedef std::vector<size_type>   size_vec_t;


/*
print a vector (with some custom settings)
*/
template <typename T>
void print_vec(const std::vector<T>& v,
               const std::string& separator, // separate vector elements with this
               const std::string& closing, // what to put between last element and closing ]
               bool newLineEnd){ // print a new line after closing ] ?
  int n = v.size();
  if (n == 0)
    std::cout << "[]";
  else{
    std::cout << "[ " << v[0];
    for (int i = 1; i < v.size(); i ++)
      std::cout << separator << v[i];
      std::cout << closing << "]";
  }
  if (newLineEnd)
    std::cout << std::endl;
}
// use spaces as default separators
template <typename T>
inline void print_vec(const std::vector<T>& v, bool newLineEnd){
  print_vec(v, ", ", " ", newLineEnd);
}
// by default, print a new line at the end
template <typename T>
inline void print_vec(const std::vector<T>& v){
  print_vec(v, ", ", " ", true);
}

/*
Return the last element of v, and 0 if v is empty
*/
template <typename T>
T last(std::vector<T> v){
  if (v.size() == 0)
    return 0;
  return v[v.size() - 1];
}

/*
The number of coefficients of P in K[X0,...,X{n-1}]
if deg(P,Xi) == d[i] for each i
*/
size_vec_t size_of_deg(deg_vec_t d){
  int n = d.size();
  size_vec_t result;
  result.resize(n+1);
  result[0] = 1;
  for (int i = 0; i < n; i++)
    result[i+1] = result[i] * (d[i] + 1);
  return result;
}

/*
The vector [ max(a[i], b[i]) for i < a.size() ], with b[i] = -Infinity if i >= b.size()
(Kronecker substitution for multivariate addition)
*/
deg_vec_t max_lazy(const deg_vec_t a, const deg_vec_t b){
  int n = a.size();
  int m = b.size();
  deg_vec_t r;
  r.resize(n);
  for (int i = 0; i < n; i++){
    if (i < m && b[i] > a[i])
      r[i] = b[i];
    else
      r[i] = a[i];
  }
  return r;
}

/*
The vector [ a[i]+b[i] for i < a.size() ], with b[i] = 0 if i >= b.size()
(Kronecker substitution for multivariate multiplication)
*/
deg_vec_t sum_lazy(const deg_vec_t a, const deg_vec_t b){
  int n = a.size();
  int m = b.size();
  deg_vec_t r = a;
  for (int i = 0; i < n; i++){
    if (i < m)
      r[i] += b[i];
  }
  return r;
}

/*
Add e at the end of v, and resize v if necessary.
*/
template <typename T>
void append(std::vector<T>& v, T& e){
  int n = v.size();
  if (n == v.capacity())
    v.reserve(2*n);
  v.push_back(e);
}

/*
The max of four elements, since I can't call std::max(initializer_list {a,b,c,d}) for some reason
*/
template <typename T>
T max_of_four(const T& a, const T& b, const T& c, const T& d){
  using std::max;
  return max(max(max(a,b),c),d);
}

#endif

