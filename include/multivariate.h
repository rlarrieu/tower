/******************************************************************************
* FILE       : multivariate.h
* DESCRIPTION: Arithmetic of multivariate polynomials
* COPYRIGHT  : (C) 2019,  Robin Larrieu
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#ifndef _TOWER_MULTIVARIATE__H_
#define _TOWER_MULTIVARIATE__H_

#include "config.h"
#include <NTL/ZZ_pX.h>
#include <NTL/lzz_pX.h>
#include <NTL/GF2X.h>
#include <NTL/ZZ_pEX.h>
#include <NTL/lzz_pEX.h>
#include <NTL/GF2EX.h>
#include "polynomial_tools.h"
#include "vector_tools.h"
#include <vector>
#include <iostream>


template <typename BaseField>
class Multivariate{
  typedef typename BaseField::poly_type Polynomial;

public:
  Polynomial repr;    // Kronecker representation
  deg_vec_t  degrees; // Degree in the variable Xi
  size_vec_t sizes;   // size of K[X0,...,X{i-1}]
  // NOTE: sizes is essentially a precomputation;
  // to avoid case distincions, sizes.size() == degrees.size()+1, and sizes[0] == 1

  /*
  Constructors
  */

  // The zero polynomial
  Multivariate(){
    repr = Polynomial();
    degrees = {};
    sizes = {1};
  }

  // The constant polynomial with coefficient a
  Multivariate(BaseField a){
    repr = Polynomial(a);
    degrees = {};
    sizes = {1};
  }

  Multivariate(long a): Multivariate(BaseField(a)) {};


  // The term a*X0^d[0]*...*Xn^d[n]
  Multivariate(BaseField a, deg_vec_t d){
    degrees = d;
    sizes = size_of_deg(d);
    if (d.size() == 0)
      repr = Polynomial(a);
    else{
      size_type s = sizes[d.size()];
      repr.SetMaxLength(s);
      NTL::SetCoeff(repr, s-1, a);
    }
  }

  Multivariate(long a, deg_vec_t d): Multivariate(BaseField(a), d) {};


  Multivariate(Polynomial r, deg_vec_t d){
    repr = r;
    degrees = d;
    sizes = size_of_deg(d);
    if (NTL::deg(repr) > last(sizes))
      NTL::trunc(repr, repr, last(sizes));
  }

  /*
  Some basic functions
  */

  int nb_vars() const { return degrees.size(); }

  size_type size() const { return last(sizes); }

  bool is_zero() const {
    long n = NTL::deg(repr)+1;
    for (long i = 0; i < n; i ++){
      if (! NTL::IsZero(NTL::coeff(repr,i)))
        return false;
    }
    return true;
  }

  bool is_constant() const {
    long n = NTL::deg(repr)+1;
    for (long i = 1; i < n; i ++){
      if (! NTL::IsZero(NTL::coeff(repr,i)))
        return false;
    }
    return true;
  }

  // reset the polynomial to zero
  void clear(){
    NTL::clear(repr);
    degrees = {};
    sizes = {1};
  }

  // swap the content with a
  void swap(Multivariate& a){
    repr.swap(a.repr);
    degrees.swap(a.degrees);
    sizes.swap(a.sizes);
  }

  /*
  Access/set coefficients
  */

  // for d = {d0,...,dk-1} and *this* in K[X0,...,X{n-1}], assuming k <= n:
  // get the coefficient c in K[X0,...,X{n-k-1}] with degree di in X{n-k+i}
  Multivariate coeff(const deg_vec_t& d) const{
    int n = nb_vars();
    int k = d.size();
    if (k > n)
      NTL::LogicError("error in coeff (number of variables)");
    if (k == 0)
      return Multivariate(repr, degrees);
    size_type offset = 0;
    for (int i = 0; i < k; i ++){
      if (d[i] < 0 || d[i] > degrees[n-k+i])
        return Multivariate();
      offset += d[i] * sizes[n-k+i];
    }
    long deg = NTL::deg(repr);
    if (offset > deg)
      return Multivariate();
    deg_vec_t degs;
    degs.resize(n-k);
    for (int i = 0; i < n-k; i++)
      degs[i] = degrees[i];
    Multivariate result(Polynomial(), degs);
    size_type s = sizes[n-k];
    if (offset + s > deg+1)
      s = deg+1-offset;
    result.repr.SetLength(s);
    copy_range(repr, offset, s, result.repr, 0);
    result.repr.normalize();
    return result;
  }

  // The coefficient of degree i (in K[X0,...,X(n-1)])
  Multivariate coeff(degree_type i) const{
    deg_vec_t d = {i}; return coeff(d);
  }

  // for d = {d0,...,dk-1} and *this* in K[X0,...,X{n-1}], assuming k <= n:
  // set c in K[X0,...,X{n-k-1}] as the coefficient with degree di in X{n-k+i}
  void setCoeff(const deg_vec_t& d, const Multivariate& c){
    int n = nb_vars();
    int k = d.size();
    int m = c.nb_vars();

    // Sanity checks (some cases could make sense, but is is certainly a mistake so better throw an exception)
    // If this is actually what you want, you should resize *this* appropriately before calling setCoeff
    if (n < m+k){
      NTL::LogicError("error in setCoeff (number of variables)");
      return;
    }
    for (int i = 0; i < m; i++){
      if (degrees[i] < c.degrees[i]){
        NTL::LogicError("error in setCoeff (degrees of the coefficient > degrees of object)\n");
      }
    }
    size_type offset = 0;
    for (int i = 0; i < k; i++){
      if (d[i] < 0 || degrees[n-k+i] < d[i])
        NTL::LogicError("error in setCoeff (target coefficient out of range)\n");
      offset += d[i] * sizes[n-k+i];
    }

    // copy c at the appropriate position
    copy_multi(repr, c.repr, n-k, offset, 0, degrees, sizes, c.degrees, c.sizes);
    // remove trailing zeroes if necessary
    repr.normalize();
  }

  // set c (in K[X0,...,X(n-1)]) as the i-th coefficient
  void setCoeff(degree_type i, const Multivariate& c){
    int n = nb_vars();
    // it is easy to increase the degree in the last variable
    if (n > 0 && i > degrees[n-1]){
      degrees[n-1] = i;
      sizes[n] = sizes[n-1] * (i+1);
    }
    deg_vec_t d; d.resize(1); d[0] = i;
    setCoeff(d, c);
  }

  /*
  The actual degree and leading coefficient of *this* as a polynomial
  in K[X0,...,X{n-2}][X{n-1}] (this may be less than degrees[n-1])
  */

  degree_type actual_degree() const{
    if (nb_vars() <= 1 || NTL::deg(repr) < 0)
      return NTL::deg(repr);
    return NTL::deg(repr) / sizes[nb_vars() - 1];
  }

  inline Multivariate lc() const{
    return coeff(actual_degree());
  }

  // adjust the degree in the last variable so degrees[n-1] == actual_degree()
  inline void normalize(){
    trunc(actual_degree() +1);
  }

  /*
  Degree in the variable Xn (possibly non tight if n+1 < nb_vars())
  */
  degree_type degree_Xn(int n) const{
    if (NTL::deg(repr) < 0)
      return -1;
    if (n+1 > nb_vars())
      return 0;
    if (n+1 < nb_vars())
      return degrees[n];
    return actual_degree();
  }

  /*
  Random generators
  */

  static Multivariate random(const deg_vec_t& d){
    Multivariate result = Multivariate(Polynomial(), d);
    NTL::random(result.repr, result.size());
    return result;
  }

  static Multivariate random_monic(const deg_vec_t& d){
    int n = d.size();
    if (n == 0 || d[n-1] == 0)
      return Multivariate(BaseField (1));
    deg_vec_t u = d;
    u[n-1] --;
    Multivariate result = random(u);
    result.setCoeff(d[n-1], Multivariate(BaseField (1)));
    return result;
  }

  /*
  Univariate polynomial in Xn
  */
  static Multivariate univariate_Xn(const Polynomial& p, int n){
    deg_vec_t d = deg_vec_t(n+1,0);
    d[n] = (degree_type) NTL::deg(p);
    return Multivariate(p,d);
  }

  /*
  Changes the Kronecker representation (as a univariate polynomial) of the object:
    X_(i+1) = X_i^degrees[i] -> X_(i+1) = X_i^v[i]
    if v[i] < degrees[i], there is a reduction modulo X_i^v[i], otherwise zeros are added.
  */
  void resizeTo(const deg_vec_t& v, Multivariate& dst) const{
    int n = v.size();
    size_vec_t newSizes = size_of_deg(v);
    if (n<=1){
      dst.degrees = v;
      dst.sizes = newSizes;
      NTL::trunc(dst.repr,repr,newSizes[n]);
      return;
    }
    if (&dst == this){
      // check if we would just chunk of contiguous memory
      for (int i = 0; i < n-1 && i < nb_vars(); i++){
        if (v[i] != degrees[i]){
          Polynomial tmp;
          copy_multi(tmp, repr, nb_vars(), 0, 0, v, newSizes, degrees, sizes);
          dst.repr.swap(tmp);
          dst.degrees = v;
          dst.sizes.swap(newSizes);
          return;
        }
      }
      // if we get here, only the number of variables changed,
      // and possibly the degree in the last variable changed
      dst.degrees = v;
      dst.sizes.swap(newSizes);
      NTL::trunc(dst.repr, dst.repr, dst.sizes[n]);
    }
    else{
      copy_multi(dst.repr, repr, nb_vars(), 0, 0, v, newSizes, degrees, sizes);
      dst.repr.normalize();
      dst.degrees = v;
      dst.sizes.swap(newSizes);
    }
  }

  void resize(const deg_vec_t& v){
    resizeTo(v, *this);
  }

  /*
  Reduction modulo X{n-1}^d (should be much faster than resize)
  */
  void trunc(degree_type d){
    int n = nb_vars();
    if (n == 0) // nothing to do
      return;
    if (d == 0){
      clear();
      return;
    }
    degrees[n-1] = d-1;
    sizes[n] = d * sizes[n-1];
    NTL::trunc(repr, repr, sizes[n]);
  }

  /*
  Division by X{n-1}^d
  */
  void rshift(degree_type d){
    int n = nb_vars();
    if (n == 0 || d < 0) // nothing to do
      return;
    if (degrees[n-1] < d){
      clear();
      return;
    }
    repr >>= d * sizes[n-1];
    degrees[n-1] -= d;
    sizes[n] = (degrees[n-1]+1) * sizes[n-1];
  }

  /*
  Multiplication by X{n-1}^d
  */
  void lshift(degree_type d){
    int n = nb_vars();
    if (n == 0 || d < 0) // nothing to do
      return;
    repr <<= d * sizes[n-1];
    degrees[n-1] += d;
    sizes[n] = (degrees[n-1]+1) * sizes[n-1];
  }

  /*
  Printers
  */

  // prints the complete representation of the object
  std::ostream& complete_repr(std::ostream& out) const{
    int n = nb_vars();
    if (n == 0){
      out << "|" << NTL::coeff(repr,0) << "|";
      return out;
    }
    size_type s = sizes[n];
    for(size_type i=0; i < s; i++){
      // print some separators to indicate change of relevant variable
      int j = 0;
      size_type u = i;
      while(j < degrees.size() && u % (degrees[j]+1) == 0){
        out << "|";
        u /= (degrees[j]+1);
        j++;
      }
      out << " " << NTL::coeff(repr,i) << " ";
    }
    // close all delimiters
    for (int i = 0; i < n; i++)
      out << "|";
    return out;
  }

  void raw_print(){ complete_repr(std::cout) << "\n"; }

  // prints the object in a more user-friendly form
  std::ostream& printable_repr(std::ostream& out) const{
    int n = nb_vars();
    if (n == 0){
      out << NTL::coeff(repr,0);
      return out;
    }
    bool printPlus = false;
    deg_vec_t v;
    v.resize(n);
    for (int i = 0; i < n; i++)
      v[i] = 0; // just to be sure
    size_type s = sizes[n];
    for(size_type i=0; i < s; i++){
      if (i > 0){
        for(int j = 0; j < n; j++){
          v[j] ++;
          if (v[j] <= degrees[j])
            break;
          v[j] = 0;
        }
      }
      BaseField c = NTL::coeff(repr,i);
      if (NTL::IsZero(c))
        continue;
      if (printPlus)
        out << " + ";
      printPlus = true;
      bool printStar = false;
      if (NTL::IsOne(c)){
        if (i==0){
          out << "1";
          continue;
        }
      }
      else{
        out << repr[i];
        printStar = true;
      }
      for(int j = 0; j < n; j++){
        if (v[j]>0){
          if (printStar)
            out << "*";
          out << "X" << j;
          if (v[j] > 1)
            out << "^" << v[j];
          printStar=true;
        }
      }
    }
    if (!printPlus) // all coefficients were zero
      out << "0";
    return out;
  }

  void pretty_print(){ printable_repr(std::cout) << "\n"; }


}; // class Multivariate

template <typename BaseField>
std::ostream& operator<<(std::ostream& out, const Multivariate<BaseField>& m) {
  //return m.complete_repr(out);
  return m.printable_repr(out);
}



#define TEMPL template <typename BaseField>
#define MULTI Multivariate<BaseField>

/*---------------------------
  Arithmetic (procedural)
---------------------------*/

TEMPL
void add(MULTI& dst, const MULTI& a, const MULTI& b){
  // dst = a+b
  MULTI tmp;
  deg_vec_t da = max_lazy(a.degrees,b.degrees);
  deg_vec_t db = max_lazy(b.degrees,a.degrees);
  if (a.nb_vars() > b.nb_vars()){
    b.resizeTo(db,tmp);
    a.resizeTo(da,dst);
  }
  else{
    a.resizeTo(da,tmp);
    b.resizeTo(db,dst);
  }
  dst.repr += tmp.repr;
}

TEMPL
void sub(MULTI& dst, const MULTI& a, const MULTI& b){
  // dst = a-b
  MULTI tmp;
  deg_vec_t da = max_lazy(a.degrees,b.degrees);
  deg_vec_t db = max_lazy(b.degrees,a.degrees);
  b.resizeTo(db,tmp);
  if (a.nb_vars() > b.nb_vars()){
    a.resizeTo(da,dst);
  }
  else{
    a.resizeTo(db,dst);
  }
  dst.repr -= tmp.repr;
}

TEMPL
void neg(MULTI& dst, const MULTI& a){
  // dst = -a
  dst.degrees = a.degrees;
  dst.sizes = a.sizes;
  dst.repr = - a.repr;
}

TEMPL
void mul(MULTI& dst, const MULTI& a, const MULTI& b){
  // dst = a*b
  MULTI tmp1;
  MULTI tmp2;
  deg_vec_t d1 = sum_lazy(a.degrees,b.degrees);
  deg_vec_t d2 = sum_lazy(b.degrees,a.degrees);
  // Kronecker substitution
  a.resizeTo(d1,tmp1);
  b.resizeTo(d2,tmp2);
  if (d1.size() < d2.size())
    dst.degrees = d2;
  else
    dst.degrees = d1;
  dst.sizes = size_of_deg(dst.degrees);
  dst.repr = tmp1.repr * tmp2.repr;
}

TEMPL
void sqr(MULTI& dst, const MULTI& a){
  // dst = a^2
  deg_vec_t d = sum_lazy(a.degrees,a.degrees);
  // Kronecker substitution
  a.resizeTo(d,dst);
  NTL::sqr(dst.repr, dst.repr);
}

/*
Unsafe arithmetic, assuming operands have the right Kronecker substitution
*/


// Verify that a and b can be added/substracted without changing the Kronecker substitution.
// Also adjust the size of the result.
TEMPL
inline void checkUnsafeKroneckerSubs(MULTI& dst, const MULTI& a, const MULTI& b){
  int n = a.nb_vars()-1; // a in K[X0,...,Xn]
  int m = b.nb_vars()-1; // b in K[X0,...,Xm]
  if (n < 0){ // a is in K
    dst.degrees = b.degrees;
    dst.sizes = b.sizes;
    return;
  }
  if (m < 0){ // b is in K
    dst.degrees = a.degrees;
    dst.sizes = a.sizes;
    return;
  }
#ifdef STRONG_SANITY_CHECK
  // a and b must have the same degree in each common variable (except possibly the last)
  for (int i = 0; i < n && i < m; i++)
    if (a.degrees[i] != b.degrees[i])
      NTL::LogicError("incompatible Kronecker substitution");
  // if b has more variables than a, it cannot have a lower degree in Xn,
  // which is the last variable of a (but higher degree is allowed)
  if ((n < m && a.degrees[n] > b.degrees[n]) ||
  // and similarly if a has more variables
    (m < n && b.degrees[m] > a.degrees[m])   )
      NTL::LogicError("incompatible Kronecker substitution");
#endif
  // set the degrees of the result to be the largest of both
  if (n < m || b.degrees[m] > a.degrees[m]){
    // NOTE: this is larger than a.degrees because
    // (m < n && b.degrees[m] > a.degrees[m]) is assumed impossible,
    // so (m <= n && b.degrees[m] > a.degrees[m]) implies m == n
    dst.degrees = b.degrees;
    dst.sizes = b.sizes;
  }
  else{
    dst.degrees = a.degrees;
    dst.sizes = a.sizes;
  }
}


// a := a+b assuming the degrees already match
TEMPL
void addUnsafe(MULTI& a, const MULTI& b){
  checkUnsafeKroneckerSubs(a,a,b);
  a.repr += b.repr;
}

// a := a-b assuming the degrees already match
TEMPL
void subUnsafeLeft(MULTI& a, const MULTI& b){
  checkUnsafeKroneckerSubs(a,a,b);
  a.repr -= b.repr;
}

// b := a-b assuming the degrees already match
TEMPL
void subUnsafeRight(const MULTI& a, MULTI& b){
  checkUnsafeKroneckerSubs(b,a,b);
  NTL::sub(b.repr, a.repr, b.repr);
}


/*---------------------------
  Arithmetic (functional)
---------------------------*/

TEMPL
inline MULTI add(const MULTI& a, const MULTI& b)
  { MULTI dst; add(dst,a,b); return dst; }

TEMPL
inline MULTI sub(const MULTI& a, const MULTI& b)
  { MULTI dst; sub(dst,a,b); return dst; }

TEMPL
inline MULTI neg(const MULTI& a)
  { MULTI dst; neg(dst,a); return dst; }

TEMPL
inline MULTI mul(const MULTI& a, const MULTI& b)
  { MULTI dst; mul(dst,a,b); return dst; }

TEMPL
inline MULTI sqr(const MULTI& a)
  { MULTI dst; sqr(dst,a); return dst; }

/*---------------------------
  Arithmetic (operators)
---------------------------*/

TEMPL
inline MULTI operator+(const MULTI& a, const MULTI& b)
  { return add(a,b); }

TEMPL
inline MULTI operator-(const MULTI& a, const MULTI& b)
  { return sub(a,b); }

TEMPL
inline MULTI operator-(const MULTI& a)
  { return neg(a); }

TEMPL
inline MULTI operator*(const MULTI& a, const MULTI& b)
  { return mul(a,b); }


TEMPL
inline MULTI operator+=(MULTI& a, const MULTI& b)
  { add(a,a,b); return a; }

TEMPL
inline MULTI operator-=(MULTI& a, const MULTI& b)
  { sub(a,a,b); return a; }

TEMPL
inline MULTI operator*=(MULTI& a, const MULTI& b)
  { mul(a,a,b); return a; }

/*---------------------------
  Promotion
---------------------------*/

// slight optimization: no need to resize
// if one operand is already in the base field

TEMPL
inline void add(MULTI& dst, const MULTI& a, const BaseField& b){
  dst = MULTI(a);
  dst.repr += b;
}
TEMPL
inline void add(MULTI& dst, const BaseField& a, const MULTI& b){
  add(dst,b,a);
}
TEMPL
inline void add(MULTI& dst, const MULTI& a, long b){
  dst = MULTI(a);
  dst.repr += b;
}
TEMPL
inline void add(MULTI& dst, long a, const MULTI& b){
  add(dst,b,a);
}


TEMPL
inline void sub(MULTI& dst, const MULTI& a, const BaseField& b){
  dst = MULTI(a);
  dst.repr -= b;
}
TEMPL
inline void sub(MULTI& dst, const BaseField& a, const MULTI& b){
  neg(dst,b);
  dst.repr += a;
}
TEMPL
inline void sub(MULTI& dst, const MULTI& a, long b){
  dst = MULTI(a);
  dst.repr -= b;
}
TEMPL
inline void sub(MULTI& dst, long a, const MULTI& b){
  neg(dst,b);
  dst.repr += a;
}


TEMPL
inline void mul(MULTI& dst, const MULTI& a, const BaseField& b){
  dst = MULTI(a);
  dst.repr *= b;
}
TEMPL
inline void mul(MULTI& dst, const BaseField& a, const MULTI& b){
  mul(dst,b,a);
}
TEMPL
inline void mul(MULTI& dst, const MULTI& a, long b){
  mul(dst,a, BaseField(b));
}
TEMPL
inline void mul(MULTI& dst, long a, const MULTI& b){
  mul(dst, b, a);
}



TEMPL
inline MULTI add(const MULTI& a, const BaseField& b)
  { MULTI dst; add(dst,a,b); return dst; }
TEMPL
inline MULTI add(const BaseField& a, const MULTI& b)
  { MULTI dst; add(dst,a,b); return dst; }
TEMPL
inline MULTI add(const MULTI& a, long b)
  { MULTI dst; add(dst,a,b); return dst; }
TEMPL
inline MULTI add(long a, const MULTI& b)
  { MULTI dst; add(dst,a,b); return dst; }


TEMPL
inline MULTI sub(const MULTI& a, const BaseField& b)
  { MULTI dst; sub(dst,a,b); return dst; }
TEMPL
inline MULTI sub(const BaseField& a, const MULTI& b)
  { MULTI dst; sub(dst,a,b); return dst; }
TEMPL
inline MULTI sub(const MULTI& a, long b)
  { MULTI dst; sub(dst,a,b); return dst; }
TEMPL
inline MULTI sub(long a, const MULTI& b)
  { MULTI dst; sub(dst,a,b); return dst; }


TEMPL
inline MULTI mul(const MULTI& a, const BaseField& b)
  { MULTI dst; mul(dst,a,b); return dst; }
TEMPL
inline MULTI mul(const BaseField& a, const MULTI& b)
  { MULTI dst; mul(dst,a,b); return dst; }
TEMPL
inline MULTI mul(const MULTI& a, long b)
  { MULTI dst; mul(dst,a,b); return dst; }
TEMPL
inline MULTI mul(long a, const MULTI& b)
  { MULTI dst; mul(dst,a,b); return dst; }

TEMPL
inline MULTI operator+(const MULTI& a, const BaseField& b)
  { MULTI dst; add(dst,a,b); return dst; }
TEMPL
inline MULTI operator+(const BaseField& a, const MULTI& b)
  { MULTI dst; add(dst,a,b); return dst; }
TEMPL
inline MULTI operator+(const MULTI& a, long b)
  { MULTI dst; add(dst,a,b); return dst; }
TEMPL
inline MULTI operator+(long a, const MULTI& b)
  { MULTI dst; add(dst,a,b); return dst; }


TEMPL
inline MULTI operator-(const MULTI& a, const BaseField& b)
  { MULTI dst; sub(dst,a,b); return dst; }
TEMPL
inline MULTI operator-(const BaseField& a, const MULTI& b)
  { MULTI dst; sub(dst,a,b); return dst; }
TEMPL
inline MULTI operator-(const MULTI& a, long b)
  { MULTI dst; sub(dst,a,b); return dst; }
TEMPL
inline MULTI operator-(long a, const MULTI& b)
  { MULTI dst; sub(dst,a,b); return dst; }


TEMPL
inline MULTI operator*(const MULTI& a, const BaseField& b)
  { MULTI dst; mul(dst,a,b); return dst; }
TEMPL
inline MULTI operator*(const BaseField& a, const MULTI& b)
  { MULTI dst; mul(dst,a,b); return dst; }
TEMPL
inline MULTI operator*(const MULTI& a, long b)
  { MULTI dst; mul(dst,a,b); return dst; }
TEMPL
inline MULTI operator*(long a, const MULTI& b)
  { MULTI dst; mul(dst,a,b); return dst; }


TEMPL
inline MULTI operator+=(MULTI& a, const BaseField& b)
  { a.repr += b; return a; }
TEMPL
inline MULTI operator-=(MULTI& a, const BaseField& b)
  { a.repr -= b; return a; }
TEMPL
inline MULTI operator*=(MULTI& a, const BaseField& b)
  { a.repr *= b; return a; }
TEMPL
inline MULTI operator+=(MULTI& a, long b)
  { a.repr += b; return a; }
TEMPL
inline MULTI operator-=(MULTI& a, long b)
  { a.repr -= b; return a; }
TEMPL
inline MULTI operator*=(MULTI& a, long b)
  { a.repr *= b; return a; }


/*---------------------------
  Comparison
---------------------------*/
TEMPL
long operator==(const MULTI& a, const MULTI& b)
  { MULTI c = a-b; return c.repr == 0; }

TEMPL
long operator==(const MULTI& a, const BaseField& b){ return a.repr == b; }
TEMPL
long operator==(const MULTI& a, long b){ return a.repr == b; }
TEMPL
long operator==(const BaseField& a, const MULTI& b){ return b == a; }
TEMPL
long operator==(long a, const MULTI& b){ return b == a; }


TEMPL
long operator!=(const MULTI& a, const MULTI& b){ return !(b == a); }
TEMPL
long operator!=(const MULTI& a, const BaseField& b){ return !(b == a); }
TEMPL
long operator!=(const MULTI& a, long b){ return !(b == a); }
TEMPL
long operator!=(const BaseField& a, const MULTI& b){ return !(b == a); }
TEMPL
long operator!=(long a, const MULTI& b) { return !(b == a); }


/*---------------------------
 Reverse the order of coeffs
---------------------------*/

TEMPL
void reverse(MULTI& dst, const MULTI& src){
  if (&dst == &src){
    MULTI tmp;
    reverse(tmp,src);
    dst.repr.swap(tmp.repr);
  }
  else{
    dst.degrees = src.degrees;
    dst.sizes = src.sizes;
    int n = src.nb_vars();
    if (n == 0){ // the polynomial is empty
      dst = MULTI();
      return;
    }
    degree_type d = src.degrees[n-1];
    if (n == 1)
      NTL::reverse(dst.repr, src.repr, d);
    else{
      dst.repr.SetLength(src.size());
      size_type s = src.sizes[n-1];
      auto end = NTL::deg(src.repr)+1;
      for(degree_type i = 0; i <= d; i ++){
        if ((i+1) * s < end)
          copy_range(src.repr,i*s,s,dst.repr,(d-i)*s);
        else{
          copy_range(src.repr,i*s,end-i*s,dst.repr,(d-i)*s);
          break;
        }
      }
      dst.repr.normalize();
    }
  }
}

TEMPL
MULTI reverse(const MULTI& src)
  { MULTI x; reverse(x, src); return x; }
#undef TEMPL
#undef MULTI

#endif
