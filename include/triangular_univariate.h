/******************************************************************************
* FILE       : triangular_univariate.h
* DESCRIPTION: Triangular systems where T_i is univariate in X_i
* COPYRIGHT  : (C) 2019,  Robin Larrieu
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#ifndef _TOWER_TRIANGULAR_UNIVARIATE__H_
#define _TOWER_TRIANGULAR_UNIVARIATE__H_

#include "config.h"
#include "multivariate.h"
#include "triangular.h"
#include <vector>
#include "vector_tools.h"

template <typename BaseField>
class TriangularUnivariate : public Triangular<BaseField>{
  typedef Multivariate<BaseField> Multi;
  typedef std::vector<Multi> System;
  typedef typename BaseField::poly_type Polynomial;
  typedef typename Polynomial::modulus_type Modulus;
  typedef std::vector<Modulus> SystemMod;
#ifdef PARALLEL_REDUCTION
  typedef typename BaseField::context_type Context;
#endif

  // Precomputed information about each Ti for faster reduction
  // (since Ti is in K[Xi], we can use NTLs structures for univariate reduction)
  SystemMod M;

public:
  TriangularUnivariate(int alloc_size): Triangular<BaseField>(alloc_size){
    M=SystemMod();
    M.reserve(alloc_size);
  }

  TriangularUnivariate(): TriangularUnivariate(TRIANGULAR_DEFAULT_ALLOC_SIZE) {}

private:
  // Compute the normal form in Kn[Xn, X{n+1},...]
#ifdef PARALLEL_REDUCTION
  void rem_rec(Multi& dst, const Multi& m, int n, bool parallel) const override{
#else
  void rem_rec(Multi& dst, const Multi& m, int n) const override{
#endif
    int vars = m.nb_vars();
#ifdef STRONG_SANITY_CHECK
    // must ensure &dst != &m
    if (n == 0){
      dst = m;
      return;
    }
    if (n > this->size())
      return rem_rec(dst, m, this->size());
    if (vars < n)
      return rem_rec(dst, m, vars);
    if (m.degrees[n-1] > 2*(this->degrees[n-1]) -1)
      NTL::LogicError("Error in rem: degree too large");
#endif
    if (vars > n){
      deg_vec_t d_rem = m.degrees; // degrees of the remainder
      deg_vec_t d (n,0); // degrees of the coefficients of m (in Kn)
      for (int i = 0; i < n; i++){
        d_rem[i] = (this->degrees[i])-1;
        d[i] = m.degrees[i];
      }
      // pre_allocate the appropriate space
      dst = Multi(Polynomial(), d_rem);
      size_type s1 = m.sizes[n]; // size of the coefficients of m
      size_type s2 = dst.sizes[n]; // size of their normal form
      // number of coefficients in Kn: ceil((deg+1) / s1)
      size_type nb_coef = (NTL::deg(m.repr)+s1) / s1;
      // use lower-level functions to speed-up the computations
      if (nb_coef > 0){
        dst.repr.SetLength(nb_coef*s2);
        fill_zeroes(dst.repr, 0, nb_coef*s2);
#ifdef PARALLEL_REDUCTION
        if (parallel){
          // parallel reduction of the coefficients
          Context context;
          context.save();
          NTL_EXEC_RANGE(nb_coef, first, last)
            context.restore();
            Multi aux1, aux2;
            aux1 = Multi(Polynomial(),d);
            if (last < nb_coef){
              for (long i = first; i < last; i++) {
                this -> thread_safe_reduce_coeff(dst, m, n, i, aux1, s1, aux2, s2, d, s1);
              }
            }
            else{
              for (long i = first; i < last-1; i++) {
                this -> thread_safe_reduce_coeff(dst, m, n, i, aux1, s1, aux2, s2, d, s1);
              }
              size_type sc = (NTL::deg(m.repr) % s1) + 1;
              this -> thread_safe_reduce_coeff(dst, m, n, nb_coef-1, aux1, s1, aux2, s2, d, sc);
            }
          NTL_EXEC_RANGE_END
        }
        else{
#endif        // sequential reduction of the coefficients
          Multi aux1, aux2;
          aux1 = Multi(Polynomial(),d);
          for (size_type i=0; i < nb_coef-1; i++){
            this -> reduce_coeff(dst, m, n, i, aux1, s1, aux2, s2, d, s1);
          }
          // handle the last coefficient
          size_type sc = (NTL::deg(m.repr) % s1) + 1;
          this -> reduce_coeff(dst, m, n, nb_coef-1, aux1, s1, aux2, s2, d, sc);
#ifdef PARALLEL_REDUCTION
        }
#endif
        dst.repr.normalize();
      }
      return;
    }
    // NOTE: 0 < n == m.nb_vars() <= T.size()
    degree_type deg = this->degrees[n-1];
    // shortcut: reduction of a univariate polynomial
    if (n == 1){
      degree_type d = deg; d--; // degrees[0]-1 may cause compiler warnings
      dst = Multi(Polynomial(), {d});
      NTL::rem(dst.repr,m.repr,M[0]);
      return;
    }
    // shortcut: input has low degree
    if (m.degrees[n-1] < deg){
      return REM_REC(dst, m, n-1);
    }
    REM_REC(dst, m, n-1); // reduce each coefficient modulo T^(-)
    NTL::rem(dst.repr, dst.repr, M[n-1]); // reduce moduo T[n-1]
    dst.trunc(deg); // adjust the degree
  }


public:
  // add a new polynomial p in K[Z] to the system as p(Xn)
  void update(const Polynomial& p){
    degree_type d = (degree_type) NTL::deg(p);
    if (d < 1)
      NTL::LogicError("Updating with a constant polynomial");
    if (NTL::coeff(p,d) != 1)
      NTL::LogicError("Trying to update with a non-monic polynomial");
    int n = this->size();
    deg_vec_t deg;
    size_type s = 1;
    deg.resize(n+1);
    for (int i = 0; i < n; i++){
      deg[i] = 0;
      s *= this->degrees[i];
    }
    deg[n] = d;
    Multi Ti = Multi(p,deg);
    Polynomial KroneckerSubs;
    append(this->T,Ti);
    for (long i = d; i >= 0; i--)
      NTL::SetCoeff(KroneckerSubs, s*i, NTL::coeff(p,i));
    Modulus Mi = Modulus(KroneckerSubs);
    append(M,Mi);
    append(this->degrees, d);
  }

private:
  void DivRemModUnsafe_Ti(Multi& q, Multi& r, int i, const Multi& b) const{
#ifdef STRONG_SANITY_CHECK
    if (b.nb_vars() != i+1 || i > this->size())
      NTL::LogicError("DivRem: inconsistent number of variables");
    if (b.degrees[i] > b.actual_degree() || b.degrees[i] > this->degrees[i])
      NTL::LogicError("DivRem: b has wrong degree");
    if (b.degrees[i] == 0)
      NTL::LogicError("DivRem: b shouldn't be a constant");
#endif
    degree_type dt = this->degrees[i];
    degree_type db = b.degrees[i];
    const Multi Ti = this->T[i];
    if (i == 0){
      // shortcut: reduction of univariate polynomials
      dt -= db;
      q = Multi(0, {dt});
      r = Multi(0, {--db});
      NTL::DivRem(q.repr, r.repr, Ti.repr, b.repr);
      return;
    }
    Multi c = b.coeff(db);
    this->InvMod(c,c);
    r = reverse(b);
    this->MulMod(r, r, c, i);
    r = this->InvModTrunc(r, dt-db+1, i);
    q = reverse(r);
    this->MulMod(q, q, c, i);
    //T[i] is univariate in Xi => no need to resize
    q.repr *= M[i].val(); // M[i].val() is T[i] with the appropriate Kronecker substitution
    q.trunc(2*dt-db+1); // adjust the degree
    // also, no need to reduce the coefficients
    q.rshift(dt); // q = q quo Xk^{dt}
    this->MulMod(r,b,q,i);
    r = Ti - r;
    r.trunc(db);
  }


};

#endif
