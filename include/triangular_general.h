/******************************************************************************
* FILE       : triangular_general.h
* DESCRIPTION: Triangular systems (general case)
* COPYRIGHT  : (C) 2019,  Robin Larrieu
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#ifndef _TOWER_TRIANGULAR_GENERAL__H_
#define _TOWER_TRIANGULAR_GENERAL__H_

#include "config.h"
#include "multivariate.h"
#include "triangular.h"
#include <vector>
#include "vector_tools.h"
#include "output_tools.h"


template <typename BaseField>
class TriangularGeneral : public Triangular<BaseField>{
  typedef Multivariate<BaseField> Multi;
  typedef std::vector<Multi> System;
  typedef typename BaseField::poly_type Polynomial;
#ifdef PARALLEL_REDUCTION
  typedef typename BaseField::context_type Context;
#endif

  // Precomputed information about each Ti for faster reduction:
  // rev(Si) with Si = rev(Ti)^(-1) mod Xi^di
  // and stored in the appropriate Kronecker form
  System S;

public:
  TriangularGeneral(int alloc_size): Triangular<BaseField>(alloc_size){
    S=System();
    S.reserve(alloc_size);
  }

  TriangularGeneral(): TriangularGeneral(TRIANGULAR_DEFAULT_ALLOC_SIZE) {}

private:
#ifdef PARALLEL_REDUCTION
  void rem_rec(Multi& dst, const Multi& m, int n, bool parallel) const override{
#else
  void rem_rec(Multi& dst, const Multi& m, int n) const override{
#endif
    int vars = m.nb_vars();
#ifdef STRONG_SANITY_CHECK
    if (&dst == m)
      NTL::LogicError("rem_rec: dst is an alias of input");
    if (n > this->size())
      return rem_rec(dst, m, this->size());
    if (vars < n)
      return rem_rec(dst, m, vars);
    if (m.degrees[n-1] > 2*(this->degrees[n-1]) -1)
      NTL::LogicError("Error in rem: degree too large");
#endif
    if (vars > n){
      deg_vec_t d_rem = m.degrees; // degrees of the remainder
      deg_vec_t d (n,0); // degrees of the coefficients of m (in Kn)
      for (int i = 0; i < n; i++){
        d_rem[i] = (this->degrees[i])-1;
        d[i] = m.degrees[i];
      }
      // pre_allocate the appropriate space
      dst = Multi(Polynomial(), d_rem);
      size_type s1 = m.sizes[n]; // size of the coefficients of m
      size_type s2 = dst.sizes[n]; // size of their normal form
      // number of coefficients in Kn: ceil((deg+1) / s1)
      size_type nb_coef = (NTL::deg(m.repr)+s1) / s1;
      // use lower-level functions to speed-up the computations
      if (nb_coef > 0){
        dst.repr.SetLength(nb_coef*s2);
        fill_zeroes(dst.repr, 0, nb_coef*s2);
#ifdef PARALLEL_REDUCTION
        if (parallel){
          // parallel reduction of the coefficients
          Context context;
          context.save();
          NTL_EXEC_RANGE(nb_coef, first, last)
            context.restore();
            Multi aux1, aux2;
            aux1 = Multi(Polynomial(),d);
            if (last < nb_coef){
              for (long i = first; i < last; i++) {
                this -> thread_safe_reduce_coeff(dst, m, n, i, aux1, s1, aux2, s2, d, s1);
              }
            }
            else{
              for (long i = first; i < last-1; i++) {
                this -> thread_safe_reduce_coeff(dst, m, n, i, aux1, s1, aux2, s2, d, s1);
              }
              size_type sc = (NTL::deg(m.repr) % s1) + 1;
              this -> thread_safe_reduce_coeff(dst, m, n, nb_coef-1, aux1, s1, aux2, s2, d, sc);
            }
          NTL_EXEC_RANGE_END
        }
        else{
#endif        // sequential reduction of the coefficients
          Multi aux1, aux2;
          aux1 = Multi(Polynomial(),d);
          for (size_type i=0; i < nb_coef-1; i++){
            this -> reduce_coeff(dst, m, n, i, aux1, s1, aux2, s2, d, s1);
          }
          // handle the last coefficient
          size_type sc = (NTL::deg(m.repr) % s1) + 1;
          this -> reduce_coeff(dst, m, n, nb_coef-1, aux1, s1, aux2, s2, d, sc);
#ifdef PARALLEL_REDUCTION
        }
#endif
      }
      dst.repr.normalize();
      return;
    }
    // NOTE: 0 < n == m.nb_vars() <= T.size()
    degree_type deg = this->degrees[n-1];
    // shortcut: reduction of a univariate polynomial
    if (n == 1){
      auto a = m.repr;
      auto p = a;
      p *= S[0].repr;
      p >>= 2*deg;
      dst = Multi(a - p * (this->T[0]).repr, {--deg});
      return;
    }
    // shortcut: input has low degree
    if (m.degrees[n-1] < deg){
      return REM_REC(dst, m, n-1);
    }
    Multi a, p, aux;


    // reduce the upper coefficients modulo T^(-)
    p = Multi(Polynomial(), m.degrees); p.trunc(deg);
    NTL::RightShift(p.repr, m.repr, deg * m.sizes[n-1]); // p = m quo X{n-1}^deg
    REM_REC(a, p, n-1); // a = p rem T^(-)
    a.lshift(deg); // a *= X{n-1}^deg

    // Kronecker substitution to prepare the multiplication
    deg_vec_t d;
    d.resize(n);
    for (int i = 0; i < n; i ++)
      d[i] = 2*(this->degrees[i]) -1;
    a.resize(d);

    // S is already in the appropriate Kronecker form
    a.repr *= S[n-1].repr;
    a.trunc(3*deg); // adjust the degree

    // note: a has degree at most 3*d(n-1)-1
    size_type sizeC = a.sizes[n-1]; // size of the coefficients in K{n-1}
    a.rshift(2*deg); // a = a quo X{n-1}^2d{n-1}
    REM_REC(p, a, n-1); // now p = m quo T{n-1} in K{n-1}[X{n-1}]

    p.resize(d);
    // T is already in the appropriate Kronecker form
    p.repr *= (this->T[n-1]).repr; // p *= T{n-1}
    p = m-p; // now p rem X{n-1}^d{n-1} = m rem T{n-1}
    p.trunc(deg); // adjust the degree
    REM_REC(dst, p, n-1); // reduce each coefficient modulo T^(-)
  }


public: // add a new polynomial to the system
  void update(const Multi& p){
    int n = this->size();
    // ensure p has 1 variable more than the system and is monic
    if (p.nb_vars() != n+1 || p.coeff(p.degrees[n]) != 1){
      // TODO: make my own exception to avoid conflicts?
      printf("#update error: p has %d variables (expected %d),\nits leading term is ", p.nb_vars(), n+1);
      std::cout << p.coeff(p.degrees[n]) << " (expected 1)\n";
      NTL::LogicError("");
    }
    // reduce p modulo T
    Multi aux;
    this->rem(aux, p,n);
    Multi Ti = aux;
    // precompute Kronecker substitution
    degree_type d = p.degrees[n];
    deg_vec_t deg;
    deg.resize(n + 1);
    for (int i = 0; i < n; i++)
      deg[i] = 2*(this->degrees[i])-1;
    deg[n] = d;
    Ti.resize(deg);
    // compute the inverse modulo Xn^d (for faster division)
    aux = reverse(aux);
    aux = this->InvModTrunc(aux, d+1,n);
    aux.resize(deg);
    aux = reverse(aux);
    // append the result
    append(this->T,Ti);
    append(S,aux);
    append(this->degrees, d);
  }

private:
  void DivRemModUnsafe_Ti(Multi& q, Multi& r, int i, const Multi& b) const{
#ifdef STRONG_SANITY_CHECK
    if (b.nb_vars() != i+1 || i > this->size())
      NTL::LogicError("DivRem: inconsistent number of variables");
    if (b.degrees[i] > b.actual_degree() || b.degrees[i] > this->degrees[i])
      NTL::LogicError("DivRem: b has wrong degree");
    if (b.degrees[i] == 0)
      NTL::LogicError("DivRem: b shouldn't be a constant");
#endif
    degree_type dt = this->degrees[i];
    degree_type db = b.degrees[i];
    const Multi Ti = this->T[i];
    if (i == 0){
      // shortcut: reduction of univariate polynomials
      dt -= db;
      q = Multi(0, {dt});
      r = Multi(0, {--db});
      NTL::DivRem(q.repr, r.repr, Ti.repr, b.repr);
      return;
    }
    Multi c = b.coeff(db);
    this->InvMod(c,c);
    r = reverse(b);
    this->MulMod(r, r, c, i);
    r = this->InvModTrunc(r, dt-db+1, i);
    q = reverse(r);
    this->MulMod(q, q, c, i);
    //T[i] has already the correct Kronecker substitution => resize only q
    deg_vec_t d = q.degrees;
    for (int j = 0; j < i; j++)
      d[j] = 2*(this->degrees[j])-1;
    d[i] += dt;
    q.resize(d);
    q.repr *= Ti.repr;
    q = this->rem(q,i); // reduce the coefficients of q modulo (T0,...T{i-1})
    q.rshift(dt); // q = q quo Xk^{dt}
    this->MulMod(r,b,q,i);
    // make a copy of Ti with normal Kronecker substitution
    for (int j = 0; j < i; j++)
      d[j] = (this->degrees[j])-1;
    d[i] = dt;
    c = Ti;
    c.resize(d);
    // r = Ti - q*b
    subUnsafeRight(c, r); // r = c-r;
    r.trunc(db);
  }

};

#endif
