/******************************************************************************
* FILE       : tensor_product.cpp
* DESCRIPTION: Test file for tensor product of composita
* COPYRIGHT  : (C) 2019,  Robin Larrieu
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#include "tensor_product.h"

int main() {
  typedef TensorProduct<NTL::zz_p> TensProd;
  NTL::zz_p::init(31);
  TensProd tens(2);

  tens.add_tower(3,1);
  tens.add_tower(5,1);
  tens.add_tower(2,0);
  tens.print();

  TensProd::Lev lev;
  lev.emplace(2, 2);
  lev.emplace(3, 1);
  lev.emplace(5, 1);
  tens.system(lev).Print();
  tens.system(lev).Print();

  TensProd::Lev from, to;
  from.emplace(2,1);
  from.emplace(3,0);
  from.emplace(5,1);
  to.emplace(2,2);
  to.emplace(3,1);
  to.emplace(5,1);

  std::vector<long> d = {1,4};
  auto x = TensProd::Multi::random(d);
  std::cout << x << "\n";
  std::cout << tens.move(x, from, to) << "\n";
}
