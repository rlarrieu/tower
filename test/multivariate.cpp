/******************************************************************************
* FILE       : multivariate.cpp
* DESCRIPTION: Test file for multivariate arithmetic
* COPYRIGHT  : (C) 2019,  Robin Larrieu
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#include "multivariate.h"
#include "output_tools.h"

typedef Multivariate<NTL::GF2>  MultiGF2;
typedef Multivariate<NTL::ZZ_p> MultiZZp;
typedef Multivariate<NTL::zz_p> Multizzp;

template <typename R>
void test(){

  sage_header2("Testing manipulation of coefficients");
  new_test_series();
  deg_vec_t d2 = {2,3,2};
  R rand = R::random(d2);
  decl_sage_var("random", rand);
  R testCoef = rand.coeff(1);

  std::cout << "# coefficient in X2^1 (in K[X0,X1])\n";
  decl_sage_var("coeff", testCoef);
  run_sage_test("random.coefficient({X2:1}) == coeff");
  std::cout << "# reverse(random)\n";
  decl_sage_var("revers", reverse(rand));
  R rand2 = R::random_monic(d2);
  decl_sage_var("random_monic", rand2);
  std::cout << "#coefficient in X2^2 (expected 1)\n";
  decl_sage_var("coeff", rand2.coeff(2));
  run_sage_test("coeff == 1");


  sage_header2("Testing resizing");
  new_test_series();
  // Increase degree in all variables
  printf("#Resizing from "); print_vec(d2);
  d2 = {4,6,2};
  printf("#to "); print_vec(d2);
  rand.resize(d2);
  decl_sage_var("resized",  rand);
  run_sage_test("resized == random");
  // Increase degree in some variables and decrease in some others
  d2 = {5,1,2};
  printf("#Resizing to "); print_vec(d2);
  rand.resize(d2);
  decl_sage_var("resized2",  rand);
  run_sage_test("resized2 == resized % (X1^2)");
  // Decrease degree in the last variable (risk of overflow)
  d2 = {5,1,1};
  printf("#Resizing to "); print_vec(d2);
  rand.resize(d2);
  decl_sage_var("resized3",  rand);
  run_sage_test("resized3 == resized2 % (X2^2)");

  sage_header2("Testing mult/div by Xn^d");
  R z = R();
  decl_sage_var("deg_of_zero",  z.actual_degree());
  run_sage_test("deg_of_zero == -1");
  rand.setCoeff(1, R(1,{}));
  rand.setCoeff(2, R());
  decl_sage_var("deg_trailing_zero",  rand.actual_degree());
  decl_sage_var("lc_trailing_zero",  rand.lc());
  run_sage_test("deg_trailing_zero == 1");
  run_sage_test("lc_trailing_zero == 1");

  d2={2,2,4};
  rand = R::random_monic(d2);
  decl_sage_var("rand",  rand);
  rand.trunc(2);
  decl_sage_var("randTrunc",  rand);
  run_sage_test("randTrunc == rand %% X2^2");
  rand.trunc(3);
  decl_sage_var("randTrunc2",  rand);
  run_sage_test("randTrunc2 == randTrunc");
  rand.trunc(0);
  decl_sage_var("randTrunc3",  rand);
  run_sage_test("randTrunc3 == 0");
  decl_sage_var("deg_randTrunc3",  rand.actual_degree());
  run_sage_test("deg_randTrunc3 == -1");

  rand = R::random_monic(d2);
  decl_sage_var("rand",  rand);
  rand.rshift(2);
  decl_sage_var("randShift",  rand);
  run_sage_test("randShift == rand // X2^2");
  rand.rshift(3);
  decl_sage_var("randShift2",  rand);
  run_sage_test("randShift2 == rand // X2^5");
  run_sage_test("randShift2 == 0");
  decl_sage_var("deg_randShift2", rand.actual_degree());
  run_sage_test("deg_randShift2 == -1");

  sage_header2("Testing arithmetic");
  sage_header3("Hand-made tests");
  new_test_series();
  // some easy tests (can be checked by hand)
  deg_vec_t d = {2,3,2,3};
  R test(1,d);
  decl_sage_var("test",  test);
  run_sage_test("test == X0^2 * X1^3 * X2^2 * X3^3");
  deg_vec_t dBis = {1,6};
  R testBis(15,dBis);
  decl_sage_var("testBis",  testBis);
  run_sage_test("testBis == 15 * X0 * X1^6");

  add(testBis, test, testBis); printf("#testBis += test\n");
  decl_sage_var("newTestBis",  testBis);
  run_sage_test("newTestBis == testBis + test");
  std::cout << "testBis = newTestBis\n";

  sub(test,test, testBis);  printf("#test -= testBis\n");
  decl_sage_var("newTest", test);
  run_sage_test("newTest == test - testBis");
  std::cout << "test = newTest\n";

  R testTer;
  mul(testTer, test, testBis); printf("#testTer = test * testBis\n");
  decl_sage_var("testTer", testTer);
  run_sage_test("testTer == test * testBis");
  sqr(testTer,testBis);  printf("#testTer = testBis^2\n");
  decl_sage_var("testTer", testTer);
  run_sage_test("testTer == testBis^2");

  // some tests with larger, random input
  sage_header3("tests with random input");
  new_test_series();
  for (int i = 0; i < 5; i ++){
    R x = R::random(d);
    R y = R::random(d2);
    decl_sage_var("x",  x);
    decl_sage_var("y",  y);
    decl_sage_var("sum",  x+y);
    run_sage_test("sum == x+y");
    decl_sage_var("dif",  x-y);
    run_sage_test("dif == x-y");
    decl_sage_var("prod",  x*y);
    run_sage_test("prod == x * y");
    x += y;
    decl_sage_var("x2",  x);
    run_sage_test("x2 == x+y");
    x -= y;
    decl_sage_var("x3",  x);
    run_sage_test("x3 == x2-y");
    x *= y;
    decl_sage_var("x4",  x);
    run_sage_test("x4 == x3*y");
  }
  // some tests with constants
  sage_header3("tests when one input is a constant");
  new_test_series();
  for (int i = 0; i < 5; i ++){
    R x = R::random(d);
    decl_sage_var("x",  x);
    decl_sage_var("sum",  x+1);
    run_sage_test("sum == x+1");
    decl_sage_var("prod",  x*2);
    run_sage_test("prod == 2*x");
    x += 1;
    decl_sage_var("x2",  x);
    run_sage_test("x2 == x+1");
    x *= 2;
    decl_sage_var("x3",  x);
    run_sage_test("x3 == 2*x2");
    R c = R(1);
    c += x;
    decl_sage_var("c",  c);
    run_sage_test("c == 1+x3");
    c = R(2);
    c *= x;
    decl_sage_var("c",  c);
    run_sage_test("c == 2*x3");
  }
}


int main(){
  start_script();

  sage_header1("Test on GF2");
  declare_poly_ring(4,2);
  test<MultiGF2>();

  NTL::ZZ_p::init(NTL::ZZ(13));
  sage_header1("Test on ZZ_p(p=13)");
  declare_poly_ring(4,13);
  test<MultiZZp>();

  NTL::zz_p::init(7);
  sage_header1("Test on zz_p(p=7)");
  declare_poly_ring(4,7);
  test<Multizzp>();

  end_script();
}
