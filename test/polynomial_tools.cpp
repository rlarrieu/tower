/******************************************************************************
* FILE       : polynomial_tools.cpp
* DESCRIPTION: Test file for polynomial utilities
* COPYRIGHT  : (C) 2019,  Robin Larrieu
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#include "polynomial_tools.h"

#include <NTL/ZZ_pX.h>
#include <NTL/lzz_pX.h>
#include <random>
#include <string>
#include "output_tools.h"



void test_copy_range(long start, long pos, long len){
  NTL::GF2X src2;
  NTL::random(src2, start + 2*len);
  NTL::GF2X dst2;
  NTL::random(dst2, pos + 2*len);
  print_range(start,len);
  std::cout << "#" << src2 << "\n";
  std::cout << "#" << dst2 << "\n";
  copy_range(src2,start,len,dst2,pos);
  dst2.normalize();
  print_range(pos, len);
  std::cout << "#" << dst2 << "\n";
}

void check_copy_range(long start, long pos, long len){
  NTL::GF2X src;
  NTL::random(src, start + 2*len);
  NTL::GF2X dst;
  NTL::random(dst, pos + 2*len);
  NTL::GF2X check = dst;
  printf("#check copy_range with start=%3d, pos=%3d, length=%3d... ", start, pos, len);
  copy_range(src,start,len,dst,pos);
  for (int i = 0; i < pos+2*len; i++){
    NTL::GF2 got, expected;
    got = NTL::coeff(dst,i);
    if (pos <= i && i < pos+len)
      expected = NTL::coeff(src,i-pos+start);
    else
      expected = NTL::coeff(check, i);
    if (got != expected){
      printf("FAIL at position %d | got %d, expected %d\n",i, got, expected);
      printf("check_copy_range_%d_%d_%d = False\n", start, pos, len );
      run_sage_test("check_copy_range_%d_%d_%d", start, pos, len );
      //std::cout << dst << "\n" << check << "\n";
      return;
    }
  }
  /*int problem = 90;
  if (start == 64 && len == 40) problem = 100;
  if (start == 64 && len == 64) problem = 124;
  printf("info: check[%d]=%d ", problem, NTL::coeff(check, problem));*/
  printf("OK\n") ;
  printf("check_copy_range_%d_%d_%d = True\n", start, pos, len );
  run_sage_test("check_copy_range_%d_%d_%d", start, pos, len );;
}

void test_fill_zeroes(long start, long len){
  NTL::GF2X dst;
  NTL::random(dst, start + 2*len);
  print_range(start,len);
  dst.normalize();
  std::cout << dst << "\n";
  fill_zeroes(dst,start,len);
  dst.normalize();
  std::cout << dst << "\n";
}

void check_fill_zeroes(long start, long len){
  NTL::GF2X dst;
  NTL::random(dst, start + 2*len);
  NTL::GF2X check = dst;
  printf("#check fill_zeroes with start=%3d, len=%3d... ", start, len);
  fill_zeroes(dst,start,len);
  for (int i = 0; i < start+2*len; i++){
    NTL::GF2 got, expected;
    got = NTL::coeff(dst,i);
    if (start <= i && i < start+len)
      expected = NTL::GF2(0);
    else
      expected = NTL::coeff(check, i);
    if (got != expected){
      printf("FAIL at position %d\n",i);
      printf("check_fill_zeroes_%d_%d = False\n", start, len);
      run_sage_test("check_fill_zeroes_%d_%d", start, len);
      return;
    }
  }
  printf("OK\n");
  printf("check_fill_zeroes_%d_%d = True\n", start, len);
  run_sage_test("check_fill_zeroes_%d_%d", start, len);
}

template <typename PolyType>
void print_poly(const PolyType& p, const std::string& name){
  std::cout << name << " = ";
  bool plus = false;
  for (long i = 0; i <= NTL::deg(p); i++){
    auto c = NTL::coeff(p,i);
    if (c != 0){
      if (plus)
        std::cout << " + ";
      if (c != 1 || i == 0){
        std::cout << c;
        if (i > 0)
          std::cout << "*";
      }
      if (i == 1)
        std::cout << "X0";
      if (i > 1)
        printf("X0^%d",i);
      plus = true;
    }
  }
  std::cout << std::endl;
}

int main(){

  NTL::ZZ_p::init(NTL::ZZ(23));

  NTL::ZZ_pX src;
  NTL::random(src, 10);
  NTL::ZZ_pX dst;
  //dst.SetMaxLength(15);
  dst.SetLength(15);
  copy_range(src,3,3,dst,8);
  std::cout << "#" << src << "\n";
  std::cout << "#" << dst << "\n";

  /*
  std::cout << "\n====== check masks ======\n";
  for (int i = 0; i < NTL_BITS_PER_LONG; i ++){
    printf("i=%d\n",i);
    std::cout << "mask1: "; print_binary(mask_before1(i));
    std::cout << "mask2: "; print_binary(mask_before2(i));
  }*/

  start_script();

  std::cout << "\nprint '====== check copy_range ======\\n'\n";
  new_test_series();

  //test_copy_range(3,8,3);
  // examples with dst more shifted than src
  check_copy_range(10,30,10); // all in one word
  check_copy_range(10,30,40); // src in one word, dst in 2 words
  check_copy_range(10,30,64); // len=64
  check_copy_range(10,30,70); // len > 64, all in 2 words
  // examples with src more shifted than dst
  check_copy_range(30,10,10); // all in one word
  check_copy_range(30,10,40); // dst in one word, src in 2 words
  check_copy_range(30,10,64); // len=64
  check_copy_range(30,10,70); // len > 64, all in 2 words
  // examples with start aligned on a word
  check_copy_range(64,30,30);
  check_copy_range(64,30,40);
  check_copy_range(64,30,64);
  check_copy_range(64,30,70);
  // examples with pos aligned on a word
  check_copy_range(10,64,40);
  check_copy_range(10,64,60);
  check_copy_range(10,64,64);
  check_copy_range(10,64,70);
  // examples where no shift is required
  check_copy_range(30,30,40);
  check_copy_range(30,30,64);
  check_copy_range(30,30,70);
  check_copy_range(64,64,40);
  check_copy_range(64,64,64);
  check_copy_range(64,64,70);
  // tests with length=0
  check_copy_range(5,10,0);
  check_copy_range(5,64,0);
  check_copy_range(64,10,0);
  check_copy_range(64,64,0);

  // tests with random_parameters
  std::random_device rd; // obtain a random number from hardware
  std::mt19937 eng(rd()); // seed the generator
  std::uniform_int_distribution<> distr(0, 128); // define the range
  //for (int i = 0; i < 100; i++)
  //  check_copy_range(distr(eng),distr(eng),distr(eng));

  std::cout << std::endl;

  std::cout << "\nprint '====== check fill_zeroes ======\\n'\n";
  new_test_series();

  check_fill_zeroes(10,30);
  check_fill_zeroes(10,70);
  check_fill_zeroes(10,64);
  check_fill_zeroes(64,30);
  check_fill_zeroes(64,70);
  check_fill_zeroes(64,128);
  // examples with lenght 0
  check_fill_zeroes(10,0);
  check_fill_zeroes(64,0);
  // and really long chunks
  check_fill_zeroes(10,1000);
  check_fill_zeroes(64,1000);
  check_fill_zeroes(10,1024);
  check_fill_zeroes(64,1024);

  //for (int i = 0; i < 100; i++)
  //  check_fill_zeroes(distr(eng),distr(eng));

  /*for (int i = 0; i < NTL_BITS_PER_LONG; i ++){
    _ntl_ulong m1 = mask_before1(i);
    _ntl_ulong m2 = mask_before2(i);
    printf("----- i=%d -----\n", i);
    std::cout << "  m1 = ";
    for (int j = 0 ; j < NTL_BITS_PER_LONG; j ++) std::cout << ((m1 >> j) & 1); std::cout << std::endl;
    std::cout << "  m2 = ";
    for (int j = 0 ; j < NTL_BITS_PER_LONG; j ++) std::cout << ((m2 >> j) & 1); std::cout << std::endl;
  }*/
  std::cout << "\nprint '====== check polyMatMul ======\\n'\n";
  declare_poly_ring(1,7);
  NTL::zz_p::init(7);
  NTL::zz_pX a,b,c,d,e,f,g,h;
  for (int i = 0; i < 5; i++){
    NTL::random(a,10);
    NTL::random(b,10);
    NTL::random(c,10);
    NTL::random(d,10);
    NTL::random(e,10);
    NTL::random(f,10);
    NTL::random(g,10);
    NTL::random(h,10);
    print_poly(a, "a");
    print_poly(b, "b");
    print_poly(c, "c");
    print_poly(d, "d");
    printf("A = Matrix(R,2,2,[a,b,c,d])\n");
    print_poly(e, "e");
    print_poly(f, "f");
    print_poly(g, "g");
    print_poly(h, "h");
    printf("B = Matrix(R,2,2,[e,f,g,h])\n");
    polyMatMul(a,b,c,d,e,f,g,h,21);
    print_poly(e, "newE");
    print_poly(f, "newF");
    print_poly(g, "newG");
    print_poly(h, "newH");
    printf("C = Matrix(R,2,2,[newE,newF,newG,newH])\n");
    run_sage_test("C == A*B");
  }
  end_script();
  return 0;
}

