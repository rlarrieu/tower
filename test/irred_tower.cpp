/******************************************************************************
* FILE       : irred_tower.cpp
* DESCRIPTION: Test file for ell-adic towers
* COPYRIGHT  : (C) 2019,  Luca De Feo
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#include "irred_tower.h"

int main(){
  NTL::zz_p::init(7);
  ElladicTower_Kummer<NTL::zz_p> tower(3);
  ElladicTower_Kummer<NTL::zz_p>::Polynomial P, Q, R;

  for (int i = 0; i < 5; i++) {
    std::cout << tower[i] << "\n";
    for (int from = 0; from <= i; from++) {
      NTL::random(P, 3);
      Q = tower.inject(P, from, i);
      R = tower.section(Q, i, from);
      assert(P == R);
    }
  }
}
