/******************************************************************************
* FILE       : triangular_general.cpp
* DESCRIPTION: Test file for triangular systems (general case)
* COPYRIGHT  : (C) 2019,  Robin Larrieu
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#include "triangular_general.h"
#include "output_tools.h"

#include "NTL/GF2XFactoring.h"
#include "NTL/lzz_pXFactoring.h"
#include "NTL/ZZ_pXFactoring.h"

#define NB_VARS 9
#define TESTS1 3
#define TESTS2 3
#define TESTS3 3

template <typename BaseField>
void test(){
  typedef Multivariate<BaseField>  Multi;
  typedef TriangularGeneral<BaseField>  Sys;

  // degree of the polynomials in the system;
  //deg_vec_t degrees = {4,2,3,2,3,2};
  deg_vec_t degrees = {2,2,3};
  Sys T = Sys();

  // multi-degree of each new element
  deg_vec_t d1 = deg_vec_t();
  d1.reserve(degrees.size());
  // multi-degree of the test polynomial to be reduced
  deg_vec_t d2 = deg_vec_t();
  deg_vec_t d3 = deg_vec_t();
  deg_vec_t d4;
  d2.reserve(degrees.size());
  d3.reserve(degrees.size());
  T.Print();
  sage_header2("Test reduction and modular multiplication");
  for (int i = 0; i < degrees.size(); i ++){
    sage_header3("test in %d variables",i+1);
    new_test_series();
    d1.push_back(degrees[i]);
    d2.push_back(2*degrees[i]-1);
    d3.push_back(degrees[i]-1);
    Multi p = Multi::random_monic(d1);
    T.update(p);
    T.Print();
    // Test classical reduction
    for(int j = 0; j < TESTS1; j++){
      Multi r = Multi::random(d2);
      decl_sage_var("random", r);
      decl_sage_var("reduced",  T.rem(r));
      run_sage_test("is_reduc(reduced, random, T)");
    }
    // Test modular multiplication
    for(int j = 0; j < TESTS2; j++){
      Multi a = Multi::random(d3);
      Multi b = Multi::random(d3);
      Multi p = T.MulMod(a,b);
      decl_sage_var("a",  a);
      decl_sage_var("b",  b);
      decl_sage_var("p",  p);
      run_sage_test("is_reduc(p, a*b, T)");
    }
    // Test reduction with one more variable (i.e. (K[X0,...,X{n-1}]/T)[Xn])
    int n = 5;
    d4 = d2; d4.push_back(n);
    for(int j = 0; j < TESTS3; j++){
      Multi r = Multi::random(d4);
      decl_sage_var("random",  r);
      decl_sage_var("reduced",  T.rem(r));
      run_sage_test("is_reduc(reduced, random, T)");
    }
    // There may be an issue if the leading coefficient is smaller than the others,
    // typically if the input is monic in Xn (data is not correctly overwritten)
    for(int j = 0; j < TESTS3; j++){
      Multi r = Multi::random_monic(d4);
      decl_sage_var("random",  r);
      decl_sage_var("reduced",  T.rem(r));
      run_sage_test("is_reduc(reduced, random, T)");
    }
    d1[i] --;
  }
}

template <typename BaseField>
void test_euclidean_div(){
  typedef Multivariate<BaseField>  Multi;
  typedef TriangularGeneral<BaseField>  Sys;
  typedef typename BaseField::poly_type Polynomial;

  // degrees must be pairwise coprime to ensure the tower is a field
  deg_vec_t degrees = {5,12};

  // degree of the test polynomials in their last variable
  degree_type degA = 10;
  degree_type degB = 4;

  // degrees of a and b (euclidean division of a by b)
  deg_vec_t da, db;
  da.reserve(degrees.size()+1);
  db.reserve(degrees.size()+1);

  sage_header2("Test euclidean division");
  Sys T = Sys();
  Polynomial p;
  for(int i = 0; i <= degrees.size(); i++){
    sage_header3("test in %d variables", i+1);
    new_test_series();
    T.Print();

    da.push_back(degA);
    db.push_back(degB);

    Multi q, r, d, s, t, a, b;

    // test euclidean division
    for (int j = 0; j < TESTS3; j ++){
      Multi a = Multi::random(da);
      Multi b = Multi::random(db);
      while(b == 0) // make sure b is nonzero
        b = Multi::random(db);
      decl_sage_var("a",  a);
      decl_sage_var("b",  b);
      T.DivRemMod(q,r,a,b);
      decl_sage_var("q",  q);
      decl_sage_var("r",  r);
      run_sage_test("is_reduc(r, a-b*q,T)");
      run_sage_test("degree(r, X%d) < degree(b, X%d)", i, i);
    }

    // test GCD
    for (int j = 0; j < TESTS3; j ++){
      a = Multi::random(da);
      b = Multi::random(db);
      while(b == 0) // make sure b is nonzero
        b = Multi::random(db);
      decl_sage_var("a",  a);
      decl_sage_var("b",  b);
      T.XGCD(d,s,t,a,b,i);
      decl_sage_var("d",  d);
      decl_sage_var("s",  s);
      decl_sage_var("t",  t);
      run_sage_test("is_reduc(d, a*s + b*t, T)");
      T.DivRemMod(q,r,a,d);
      decl_sage_var("qa",  q);
      T.DivRemMod(q,r,b,d);
      decl_sage_var("qb",  q);
      run_sage_test("is_reduc(a, qa*d,T) and is_reduc(b, qb*d,T)");
    }

    if (i == degrees.size())
      break;
    NTL::BuildIrred(p,degrees[i]);
    T.update(Multi::univariate_Xn(p,i));
    T.Print();
    printf("Ti = T[%d]\n",i);
    // test euclidean division of Ti by b
    db[i] = (2*degrees[i]) / 3;
    for (int j = 0; j < TESTS3; j++){
      b = Multi::random(db);
      while(b == 0) // make sure b is nonzero
        b = Multi::random(db);
      decl_sage_var("b",  b);
      T.DivRemMod_Ti(q,r,i,b);
      decl_sage_var("q",  q);
      decl_sage_var("r",  r);
      run_sage_test("is_reduc(r, Ti-b*q,T)");
      run_sage_test("degree(r, X%d) < degree(b, X%d)",i,i);
    }

    printf("T.append(Ti)\n");
    da[i] = 1;
    // test inversion with low degree in Xn
    for (int j = 0; j < TESTS3; j++){
      a = Multi::random(da);
      while(a == 0) // make sure a is nonzero
        a = Multi::random(da);
      decl_sage_var("a",  a);
      T.InvMod(b,a);
      decl_sage_var("b",  b);
      run_sage_test("is_reduc(1, a*b,T)");
    }

    da[i] = degrees[i] -1;
    db[i] = degrees[i] -1;
    // test inversion with normal degree in Xn
    for (int j = 0; j < TESTS3; j++){
      a = Multi::random(da);
      while(a == 0) // make sure a is nonzero
        a = Multi::random(da);
      decl_sage_var("a",  a);
      T.InvMod(b,a);
      decl_sage_var("b",  b);
      run_sage_test("is_reduc(1, a*b,T)");
    }
  }
}

int main(){
  start_script();
  sage_header1("Test on GF2");
  declare_poly_ring(NB_VARS+1,2);
  test_euclidean_div<NTL::GF2>();
  NTL::zz_p::init(7);
  sage_header1("Test on zz_p(p=7)");
  declare_poly_ring(NB_VARS+1,7);
  test<NTL::zz_p>();
  test_euclidean_div<NTL::zz_p>();
  NTL::ZZ_p::init(NTL::ZZ(7));
  sage_header1("Test on ZZ_p(p=7)");
  declare_poly_ring(NB_VARS+1,7);
  test<NTL::ZZ_p>();
  test_euclidean_div<NTL::ZZ_p>();
  end_script();
  return 0;
}
