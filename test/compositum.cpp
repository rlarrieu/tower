/******************************************************************************
* FILE       : compositum.cpp
* DESCRIPTION: Test file for composita of ell-adic towers
* COPYRIGHT  : (C) 2019,  Robin Larrieu
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#include "compositum.h"

int main() {
  NTL::zz_p::init(31);
  Compositum<NTL::zz_p> comp;
  typename Compositum<NTL::zz_p>::Levels lev, lev_to;

  comp.add_tower(3);
  comp.add_tower(5);

  lev.emplace(3, 1);
  lev.emplace(5, 1);

  std::cout << comp.nb_towers() << "\n";
  std::cout << comp.modulus(lev) << "\n";
  std::cout << comp.modulus(lev) << "\n";

  Compositum<NTL::zz_p>::Polynomial x, y;
  random(x, 14);
  std::cout << x << "\n";

  lev_to.emplace(3, 2);
  lev_to.emplace(5, 2);
  y = comp.move(x, lev, lev_to);
  std::cout << y << "\n";

  y = comp.move(y, lev_to, lev);
  std::cout << y << "\n";

  assert(x == y);
}
