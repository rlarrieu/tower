/******************************************************************************
* FILE       : triangular_univariate.cpp
* DESCRIPTION: Bench file for triangular systems (univariate polynomials)
* COPYRIGHT  : (C) 2019,  Robin Larrieu
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#include "triangular_univariate.h"
#include "output_tools.h"

#include "NTL/GF2XFactoring.h"
#include "NTL/lzz_pXFactoring.h"
#include "NTL/ZZ_pXFactoring.h"

#define BENCH_MULMOD 200
#define BENCH_INVMOD 100


template <typename BaseField>
void bench_MulMod(int nb_vars, int degree){
  printf("\n");
  header3("Bench modular multiplication");
  printf("  (degree %d in %d variables)\n", degree, nb_vars);
  typedef Multivariate<BaseField>  Multi;
  typedef TriangularUnivariate<BaseField>  Sys;
  typedef typename BaseField::poly_type Polynomial;

  Sys T = Sys();
  printf("Generating triangular system of %d polynomials with degree %d\n", nb_vars, degree);
  std::cout << std::flush;
  for (int i = 0; i < nb_vars; i++){
    Polynomial p;
    NTL::random(p, degree);
    NTL::SetCoeff(p, degree, BaseField(1));
    T.update(p);
  }
  printf("Generating 2 test polynomials\n");
  deg_vec_t deg_red = deg_vec_t(nb_vars, degree-1);
  Multi a = Multi::random(deg_red);
  Multi b = Multi::random(deg_red);
  printf("Starting bench (repeatedly doing a *= b mod T)\n");
  chrono beg = now();
  for (int i = 0; i < BENCH_MULMOD; i++){
    T.MulMod(a,a,b);
  }
  chrono end = now();
  print_bench_result(beg,end,BENCH_MULMOD);
  bench_univariate_mulmod<Polynomial>(nb_vars, degree, BENCH_MULMOD);
}

template <typename BaseField>
void bench_InvMod(const deg_vec_t& deg){
  printf("\n");
  header3("Bench modular inversion");
  long dimension = 1;
  deg_vec_t deg_red = deg;
  for (int i = 0; i < deg.size(); i++){
    dimension *= deg[i];
    deg_red[i] --;
  }
  std::cout << "   (degrees "; print_vec(deg, false);
  printf(", dimension is %d)\n", dimension);
  typedef Multivariate<BaseField>  Multi;
  typedef TriangularUnivariate<BaseField>  Sys;
  typedef typename BaseField::poly_type Polynomial;

  Sys T = Sys();
  printf("Generating triangular system\n");
  std::cout << std::flush;
  for (int i = 0; i < deg.size(); i++){
    Polynomial p;
    NTL::BuildIrred(p,deg[i]);
    T.update(p);
  }
  printf("Generating test polynomials\n");
  Multi a = Multi::random(deg_red);
  Multi b = Multi::random(deg_red);
  printf("Starting bench (repeatedly doing a = b/a mod T)\n");
  printf("Note: multiply by b at each step to ensure sufficient randomness\n");
  chrono beg = now();
  for (int i = 0; i < BENCH_INVMOD; i++){
    T.InvMod(a,a);
    T.MulMod(a,a,b);
  }
  chrono end = now();
  print_bench_result(beg,end,BENCH_INVMOD);
  beg = now();
  for (int i = 0; i < BENCH_INVMOD; i++){
    T.MulMod(a,a,b);
  }
  end = now();
  print_bench_result2(beg,end,BENCH_INVMOD);
  bench_univariate_invmod<Polynomial>(dimension, BENCH_INVMOD);
}

template <typename BaseField>
void bench(){
    header2("Benchmarks for a Triangular system of univariate polynomial");
    bench_MulMod<BaseField>(10,2);
    bench_MulMod<BaseField>(5,2);
    bench_MulMod<BaseField>(3,50);
    bench_MulMod<BaseField>(2,50);
    bench_InvMod<BaseField>({2,3,5,7,11});
    bench_InvMod<BaseField>({13,15,17});
    bench_InvMod<BaseField>({29,30,31});
    bench_InvMod<BaseField>({29,30});
}

int main(){
    header1("Bench for GF2");
    bench<NTL::GF2>();

    NTL::zz_p::init(7);
    header1("Bench for zz_p(p=7)");
    bench<NTL::zz_p>();

    NTL::ZZ_p::init(NTL::ZZ(7));
    header1("Bench for ZZ_p(p=7)");
    bench<NTL::ZZ_p>();
    return 0;
}
