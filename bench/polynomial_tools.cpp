/******************************************************************************
* FILE       : polynomial_tools.cpp
* DESCRIPTION: Bench file for polynomial utilities
* COPYRIGHT  : (C) 2019,  Robin Larrieu
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#include "polynomial_tools.h"

#include <NTL/ZZ_pX.h>
#include <NTL/lzz_pX.h>
#include <random>
#include <string>
#include "output_tools.h"


#define NB_RUNS 10000
void bench_copy_range(long length){
  std::random_device rd; // obtain a random number from hardware
  std::mt19937 eng(rd()); // seed the generator
  std::uniform_int_distribution<> distr(0, 4*length-1); // define the range

  NTL::GF2X src, dst;
  NTL::random(src, 5*length);
  NTL::random(dst, 5*length);
  chrono beg = now();
  for(int i=0; i < NB_RUNS; i++){
    // copy length bits from a random location to a random location
    copy_range(src, distr(eng), length, dst, distr(eng));
  }
  chrono end = now();
  printf("Bench copy_range for length %d\n", length);
  printf("  copy_range:       ");
  print_bench_result(beg,end,NB_RUNS);
  beg = now();
  for(int i=0; i < NB_RUNS; i++){
    // copy length bits from a random location to a random location
    copy_range_naive(src, distr(eng), length, dst, distr(eng));
  }
  end = now();
  printf("  copy_range_naive: ");
  print_bench_result(beg,end,NB_RUNS);
  std::ostream dummy(0); // to avoid printing unnecessary output
  dummy << "  degree(dst)=" << NTL::deg(dst) << " (to ensure we actually ran the code)\n";
}

int main(){

  bench_copy_range(2);
  bench_copy_range(3);
  bench_copy_range(10);
  bench_copy_range(100);
  bench_copy_range(1000);
  bench_copy_range(10000);

  return 0;
}

