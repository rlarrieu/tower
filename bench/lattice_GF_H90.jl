###############################################################################
# FILE       : lattice_GF_H90.jl
# DESCRIPTION: Bench file for embeddings using standard lattices
# COPYRIGHT  : (C) 2019,  Robin Larrieu
#              Modification of the original file by Edouard Rousseau
#              (2019, https://github.com/erou/LatticeGFH90.jl)
###############################################################################
#   This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 2 of the License, or
#  (at your option) any later version.
#                   http://www.gnu.org/licenses/
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
###############################################################################

using Nemo, LatticeGFH90
import LatticeGFH90: level, ZETAS, H90_ELEMENTS, tensor_algebra, solve_h90

logfile = joinpath(dirname(@__DIR__),"log/bench_lattice_GF_H90.log")


function bench_all(p, N)

    io = open(logfile, "w+")
    write(io, string("#Benchmarks for p = ",p,"\n"))
    write(io, "# (all timings in seconds)\n\n")
    make_zetas_conway(p)

    write(io, "#Construction of the finite fields\n")
    write(io, "# degree, time\n\n")
    # Compute the H90 sol in A_l with l <= N
    for j in 1:N
        if j%p == 0
            continue
        end
        a = level(j, p)
        if haskey(ZETAS, (p, a))

            println(j)
            k, x = FiniteField(p, j, "x")
            A, t = (@timed tensor_algebra(k))[1:2]
            h, u = (@timed solve_h90(A))[1:2]
            write(io, string(degree(A), ",", t+u, "\n"))
            H90_ELEMENTS[(p, j)] = h
        end
    end


    write(io, "\n\n#Computation of the embeddings\n")
    write(io, "# degree_src, degree_dst, time_compute, time_eval\n\n")

    # Sort the A_l
    degrees = sort!(Int[y for (x,y) in keys(H90_ELEMENTS) if x == p])
    len = length(degrees)

    # Make all the possible embeddings
    for j in 1:len
        for i in (j+1):len
            l, m = degrees[j], degrees[i]
            if m % l == 0
                println(string(l, " ... ", m))
                k, x = FiniteField(p, l, "x")
                K, y = FiniteField(p, m, "y")
                a, b = level(l, p), level(m, p)

                f, v = @timed embed(k, K)
                r = rand(k)
                w = @elapsed f(r)
                write(io, string(l, ",", m, ",", v, ",", w, "\n"))
            end
        end
    end
    close(io)
end

bench_all(2311,400)
