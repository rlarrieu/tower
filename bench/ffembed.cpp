/******************************************************************************
* FILE       : ffembed.cpp
* DESCRIPTION: Bench file for finite field embeddings
* COPYRIGHT  : (C) 2019,  Robin Larrieu
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#include "tensor_product.h"
#include "output_tools.h"
#include "NTL/tools.h"
#include <bits/stdc++.h>

#define NB_RUNS 100

typedef TensorProduct<NTL::zz_p> TensProd;
typedef TensProd::Lev Levels;

const std::vector<long> factors({2,3,5,7,11});

// The degrees (up to N) of extensions of F_2311 that are supported by the current implementation
// i.e. the degrees whose prime factors are 2,3,5,7,11
std::vector<long> admissible_degrees_2311(long N) {
  std::vector<long> result;
  if (N<1)
    return result;
  result.reserve(N);
  result.push_back(1);
  for (long f: factors){
    long n = result.size();
    for (long p = f; p <=N; p *= f) {
      for (int i = 0; i < n ; i++) {
        if (p*result[i] <= N)
          result.push_back(p*result[i]);
      }
    }
  }
  std::sort(result.begin(),result.end());
  return result;
}

// get the powers of 2,3,5,7,11 in the factorization of d
Levels factor_degree(long d) {
  Levels result;
  for (long f: factors){
    long e = 0;
    long p = 1;
    while (d % (f*p) == 0){
      p *= f;
      e++;
    }
    if (e>0)
      result.emplace(f,e);
  }
  return result;
}

void bench(TensProd& tens, long N) {
  header1("Start bench");

  header3("Parameters");
  printf("\nBase field is F_%d\n",NTL::zz_p::modulus());
  printf("Extensions represented by:\n");
  tens.print();
  printf("\n");

  header3("Constructing field representations");
  printf("\n");
  std::vector<long> degrees = admissible_degrees_2311(N);
  for (long d: degrees) {
    chrono beg = now();
    auto sys = tens.system(factor_degree(d));
    chrono end = now();
    printf("Degree %d: %.4f ms\n", d, duration_ms(beg,end));
  }

  printf("\n");
  header3("Embedding");
  printf("\n");
  for (int i = 0; i+1 < degrees.size(); i++) {
    long d = degrees[i];
    Levels from = factor_degree(d);
    auto sys = tens.system(from);
    deg_vec_t deg = sys.degrees_NF();
    auto input = TensProd::Multi::random(deg);
    for (int j = i+1; j < degrees.size(); j++) {
      if (degrees[j] % d == 0){
        Levels to = factor_degree(degrees[j]);
        chrono beg = now();
        for (int r = 0; r < NB_RUNS; r ++)
          auto output = tens.move(input, from, to);
        chrono end = now();
        double time = duration_ms(beg,end) / (double) NB_RUNS;
        printf("%d -> %d: %.4f ms\n", d, degrees[j], time);
      }
    }
  }
}

// Tensor of composita (2) (3) (5) (7) (11)
TensProd five(){
  TensProd tens(5);
  tens.add_tower( 2,0);
  tens.add_tower( 3,1);
  tens.add_tower( 5,2);
  tens.add_tower( 7,3);
  tens.add_tower(11,4);
  return tens;
}

// Tensor of composita (2 3) (5) (7) (11)
TensProd four(){
  TensProd tens(4);
  tens.add_tower( 2,0);
  tens.add_tower( 3,0);
  tens.add_tower( 5,1);
  tens.add_tower( 7,2);
  tens.add_tower(11,3);
  return tens;
}

// Tensor of composita (2 3 5) (7) (11)
TensProd three1(){
  TensProd tens(3);
  tens.add_tower( 2,0);
  tens.add_tower( 3,0);
  tens.add_tower( 5,0);
  tens.add_tower( 7,1);
  tens.add_tower(11,2);
  return tens;
}

// Tensor of composita (2 5) (3 7) (11)
TensProd three2(){
  TensProd tens(3);
  tens.add_tower( 2,0);
  tens.add_tower( 3,1);
  tens.add_tower( 5,0);
  tens.add_tower( 7,1);
  tens.add_tower(11,2);
  return tens;
}

// Tensor of composita (2 3 5 7) (11)
TensProd two1(){
  TensProd tens(2);
  tens.add_tower( 2,0);
  tens.add_tower( 3,0);
  tens.add_tower( 5,0);
  tens.add_tower( 7,0);
  tens.add_tower(11,1);
  return tens;
}

// Tensor of composita (2 3 5) (7 11)
TensProd two2(){
  TensProd tens(2);
  tens.add_tower( 2,0);
  tens.add_tower( 3,0);
  tens.add_tower( 5,0);
  tens.add_tower( 7,1);
  tens.add_tower(11,1);
  return tens;
}

// Tensor of composita (2 5 11) (3 7)
TensProd two3(){
  TensProd tens(2);
  tens.add_tower( 2,0);
  tens.add_tower( 3,1);
  tens.add_tower( 5,0);
  tens.add_tower( 7,1);
  tens.add_tower(11,0);
  return tens;
}

// Tensor of composita (2 3 5 7 11)
TensProd one(){
  TensProd tens(1);
  tens.add_tower( 2,0);
  tens.add_tower( 3,0);
  tens.add_tower( 5,0);
  tens.add_tower( 7,0);
  tens.add_tower(11,0);
  return tens;
}



int main() {
  NTL::zz_p::init(2311); // 2311 = 2*3*5*7*11 + 1
  TensProd _5var  = five();
  TensProd _4var  = four();
  TensProd _3var1 = three1();
  TensProd _3var2 = three2();
  TensProd _2var1 = two1();
  TensProd _2var2 = two2();
  TensProd _2var3 = two3();
  TensProd _1var  = one();
  long N = 400;
  bench(_5var,  N);
  bench(_4var,  N);
  bench(_3var1, N);
  bench(_3var2, N);
  bench(_2var1, N);
  bench(_2var2, N);
  bench(_2var3, N);
  bench(_1var,  N);
}
