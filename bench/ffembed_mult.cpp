/******************************************************************************
* FILE       : ffembed_mult.cpp
* DESCRIPTION: Bench file for finite field embeddings + multiplication
* COPYRIGHT  : (C) 2019,  Robin Larrieu
*******************************************************************************
*   This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 2 of the License, or
*  (at your option) any later version.
*                   http://www.gnu.org/licenses/
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
******************************************************************************/

#include "tensor_product.h"
#include "output_tools.h"

typedef TensorProduct<NTL::zz_p> TensProd;
typedef TensProd::Lev Levels;

void bench(TensProd& tens, const std::vector<Levels>& lev) {
  header1("Start bench");

  header3("Parameters");
  printf("\nBase field is F_%d\n",NTL::zz_p::modulus());
  printf("Extensions represented by:\n");
  tens.print();
  printf("Input1 in ext. "); print_levels(lev[0]);
  printf("Input2 in ext. "); print_levels(lev[1]);
  printf("Output in ext. "); print_levels(lev[2]);

  printf("\n");
  header3("Constructing field representations");
  chrono beg = now();
  auto from1Sys = tens.system(lev[0]);
  chrono sys1 = now();
  auto from2Sys = tens.system(lev[1]);
  chrono sys2 = now();
  auto toSys = tens.system(lev[2]);
  chrono sys3 = now();
  printf("\nInput1: %.2f ms\nInput2: %.2f ms\nOutput: %.2f ms\n",
         duration_ms(beg,  sys1),
         duration_ms(sys1, sys2),
         duration_ms(sys2, sys3));
  printf("Total construction: %.2f ms\n\n",duration_ms(beg, sys3));

  // generating random elements
  deg_vec_t d1 = from1Sys.degrees_NF();
  auto input1 = TensProd::Multi::random(d1);

  deg_vec_t d2 = from2Sys.degrees_NF();
  auto input2 = TensProd::Multi::random(d2);

  header3("Embedding");
  beg = now();
  auto input1move = tens.move(input1, lev[0], lev[2]);
  chrono in1 = now();
  auto input2move = tens.move(input2, lev[1], lev[2]);
  chrono in2 = now();
  printf("\nOperand1: %.2f ms\nOperand2: %.2f ms\nTotal: %.2f ms\n\n",
         duration_ms(beg, in1),
         duration_ms(in1, in2),
         duration_ms(beg, in2));

  header3("Multiplication");
  toSys.MulMod(input1move,input2move);
  chrono end = now();
  printf("\nMulMod: %.2f ms\nTotal including conversions: %.2f ms\n\n",
         duration_ms(in2,end),
         duration_ms(beg,end));
}

void bench_univariate(const std::vector<Levels>& lev) {
  typedef NTL::zz_pX Poly;
  typedef NTL::zz_pXModulus Modulus;
  header1("Start bench");

  header3("Parameters");
  printf("\nBase field is F_%d\n",NTL::zz_p::modulus());
  printf("Extensions in univariate representation\n");
  printf("Input1 in ext. "); print_levels(lev[0]);
  printf("Input2 in ext. "); print_levels(lev[1]);
  printf("Output in ext. "); print_levels(lev[2]);

  printf("\n");
  header3("Constructing field representations");
  TensProd::Comp comp;
  for (auto l: lev[2])
    comp.add_tower(l.first);

  chrono beg = now();
  Poly from1Pol = comp.modulus(lev[0]);
  Modulus from1Mod(from1Pol);
  chrono mod1 = now();
  Poly from2Pol = comp.modulus(lev[1]);
  Modulus from2Mod(from2Pol);
  chrono mod2 = now();
  Poly toPol = comp.modulus(lev[2]);
  Modulus toMod(toPol);
  chrono mod3 = now();
  printf("\nInput1: %.2f ms\nInput2: %.2f ms\nOutput: %.2f ms\n",
         duration_ms(beg,  mod1),
         duration_ms(mod1, mod2),
         duration_ms(mod2, mod3));
  printf("Total construction: %.2f ms\n\n",duration_ms(beg, mod3));

  // generating random elements
  Poly input1 = NTL::random_zz_pX(deg(from1Pol)-1);
  Poly input2 = NTL::random_zz_pX(deg(from2Pol)-1);

  header3("Embedding");
  beg = now();
  Poly input1move = comp.move(input1, lev[0], lev[2]);
  chrono in1 = now();
  Poly input2move = comp.move(input2, lev[1], lev[2]);
  chrono in2 = now();
  printf("\nOperand1: %.2f ms\nOperand2: %.2f ms\nTotal: %.2f ms\n\n",
         duration_ms(beg, in1),
         duration_ms(in1, in2),
         duration_ms(beg, in2));

  header3("Multiplication");
  Poly out = NTL::MulMod(input1move,input2move,toMod);
  chrono end = now();
  printf("\nMulMod: %.2f ms\nTotal including conversions: %.2f ms\n\n",
         duration_ms(in2,end),
         duration_ms(beg,end));
}


// Tensor of composita (2) (3) (5) (7) (11)
TensProd five(){
  TensProd tens(5);
  tens.add_tower( 2,0);
  tens.add_tower( 3,1);
  tens.add_tower( 5,2);
  tens.add_tower( 7,3);
  tens.add_tower(11,4);
  return tens;
}

// Tensor of composita (2 3) (5) (7) (11)
TensProd four(){
  TensProd tens(4);
  tens.add_tower( 2,0);
  tens.add_tower( 3,0);
  tens.add_tower( 5,1);
  tens.add_tower( 7,2);
  tens.add_tower(11,3);
  return tens;
}

// Tensor of composita (2 3 5) (7) (11)
TensProd three1(){
  TensProd tens(3);
  tens.add_tower( 2,0);
  tens.add_tower( 3,0);
  tens.add_tower( 5,0);
  tens.add_tower( 7,1);
  tens.add_tower(11,2);
  return tens;
}

// Tensor of composita (2 5) (3 7) (11)
TensProd three2(){
  TensProd tens(3);
  tens.add_tower( 2,0);
  tens.add_tower( 3,1);
  tens.add_tower( 5,0);
  tens.add_tower( 7,1);
  tens.add_tower(11,2);
  return tens;
}

// Tensor of composita (2 3 5 7) (11)
TensProd two1(){
  TensProd tens(2);
  tens.add_tower( 2,0);
  tens.add_tower( 3,0);
  tens.add_tower( 5,0);
  tens.add_tower( 7,0);
  tens.add_tower(11,1);
  return tens;
}

// Tensor of composita (2 3 5) (7 11)
TensProd two2(){
  TensProd tens(2);
  tens.add_tower( 2,0);
  tens.add_tower( 3,0);
  tens.add_tower( 5,0);
  tens.add_tower( 7,1);
  tens.add_tower(11,1);
  return tens;
}

// Tensor of composita (2 5 11) (3 7)
TensProd two3(){
  TensProd tens(2);
  tens.add_tower( 2,0);
  tens.add_tower( 3,1);
  tens.add_tower( 5,0);
  tens.add_tower( 7,1);
  tens.add_tower(11,0);
  return tens;
}

// Tensor of composita (2 3 5 7 11)
TensProd one(){
  TensProd tens(1);
  tens.add_tower( 2,0);
  tens.add_tower( 3,0);
  tens.add_tower( 5,0);
  tens.add_tower( 7,0);
  tens.add_tower(11,0);
  return tens;
}

// Simple case: different primes for each operand
std::vector<Levels> simpleMult() {
  std::vector<Levels> lev(3);
  // Input1: 2^2 * 5^2 * 11
  lev[0].emplace(2,2);
  lev[0].emplace(5,2);
  lev[0].emplace(11,1);
  // Input2: 3^2 * 7^2
  lev[1].emplace(3,2);
  lev[1].emplace(7,2);
  // Output: 2^2 * 3^2 * 5^2 * 7^2 * 11
  lev[2].emplace(2,2);
  lev[2].emplace(5,2);
  lev[2].emplace(11,1);
  lev[2].emplace(3,2);
  lev[2].emplace(7,2);
  return lev;
}

// More expensive case: force conversion in each tower
// and reduction in each variable during MulMod
std::vector<Levels> expensiveMult() {
  std::vector<Levels> lev(3);
  // Input1: 2^2 * 3 * 5^2 * 7 * 11
  lev[0].emplace(2,2);
  lev[0].emplace(3,1);
  lev[0].emplace(5,2);
  lev[0].emplace(7,1);
  lev[0].emplace(11,1);
  // Input2: 2 * 3^2 * 5 * 7^2 * 11
  lev[1].emplace(2,1);
  lev[1].emplace(3,2);
  lev[1].emplace(5,1);
  lev[1].emplace(7,2);
  lev[1].emplace(11,1);
  // Output: 2^2 * 3^2 * 5^2 * 7^2 * 11
  lev[2].emplace(2,2);
  lev[2].emplace(5,2);
  lev[2].emplace(11,1);
  lev[2].emplace(3,2);
  lev[2].emplace(7,2);
  return lev;
}


int main() {
  NTL::zz_p::init(2311); // 2311 = 2*3*5*7*11 + 1
  TensProd _5var  = five();
  TensProd _4var  = four();
  TensProd _3var1 = three1();
  TensProd _3var2 = three2();
  TensProd _2var1 = two1();
  TensProd _2var2 = two2();
  TensProd _2var3 = two3();
  TensProd _1var  = one();
  auto lev = simpleMult();
  bench(_5var,  lev);
  bench(_4var,  lev);
  bench(_3var1, lev);
  bench(_3var2, lev);
  bench(_2var1, lev);
  bench(_2var2, lev);
  bench(_2var3, lev);
  bench(_1var,  lev);
  bench_univariate(lev);

  // regenerate the tensor products to clear the cache
  _5var  = five();
  _4var  = four();
  _3var1 = three1();
  _3var2 = three2();
  _2var1 = two1();
  _2var2 = two2();
  _2var3 = two3();
  _1var  = one();
  lev = expensiveMult();
  bench(_5var,  lev);
  bench(_4var,  lev);
  bench(_3var1, lev);
  bench(_3var2, lev);
  bench(_2var1, lev);
  bench(_2var2, lev);
  bench(_2var3, lev);
  bench(_1var,  lev);
  bench_univariate(lev);
}
