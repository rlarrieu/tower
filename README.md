# tower

An experimental implementation for finite fields embeddings, inspired by
the paper *Accelerated tower arithmetic* (van der Hoeven and Lecerf, 2018)



## Usage

The project can be compiled with the usual

    ./configure [--with-ntl=/path/to/ntl]
    make

(using the option `--with-ntl=/path/to/ntl` if ntl is installed
in a non-standard location)


#### Structure of the project

The main algorithms are implemented in the header files in the `include/`
directory.

Tests and benchmarks programs are found in the `test/` and `bench/` directories.


#### Running tests

The tests for program blah can be run using

    make test_blah

The output is then written in the `log/` directory.
Some tests return a valid SageMath script `log/test_blah.sage`; this allows
the results to be checked with SageMath as trusted computer algebra system.
The other test programs output a classical plain text log file
`log/test_blah.log`.


#### Running benchmarks

Similarly, the benchmarks for program blah can be run using

    make bench_blah

and the output is then written `log/bench_blah.log`.


#### Comparison with standard lattices

For comparison, we provide a Julia script to bench another
construction based on standard lattices of field embeddings
(paper: *Standard lattices of compatibly embedded finite fields*
-- De Feo, Randriam, Rousseau |
Source code <https://github.com/erou/LatticeGFH90.jl> )

First, install the required Julia packages:
start a Julia session and type `]` to enter package mode,
then type the following commands and `Backspace` to exit:

    add Nemo
    add "https://github.com/erou/LatticeGFH90.jl"

Now you may exit the Julia session, and run the script
`bench/lattice_GF_H90.jl` from the shell:

    julia bench/lattice_GF_H90.jl

The output is written in `log/bench_lattice_GF_H90.log`.

The file `bench/lattice_GF_H90.jl` is a modification of
`benchmarks.jl` from <https://github.com/erou/LatticeGFH90.jl>
by Edouard Rousseau;
it was adapted to run the same experiment as `bench/ffembed.cpp`.



## License

Copyright (C): 2019,  Robin Larrieu and Luca De Feo

You may use, copy, modify and redistribute this package
under the terms of the GNU General Public License, version 2 or later.
See the given LICENSE file for more details.
